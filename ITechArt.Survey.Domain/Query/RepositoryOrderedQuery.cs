﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ITechArt.Survey.Domain.Interfaces;

namespace ITechArt.Survey.Infra.Data.Query
{
    public class RepositoryOrderedQuery<TEntity> : RepositoryQuery<TEntity>, IOrderedQuery<TEntity>
        where TEntity : class
    {
        public RepositoryOrderedQuery(IOrderedQueryable<TEntity> query) : base(query)
        {
        }

        public IQuery<TEntity> ThenBy<TKey>(System.Linq.Expressions.Expression<Func<TEntity, TKey>> keySelector)
        {
            return new RepositoryOrderedQuery<TEntity>(((IOrderedQueryable<TEntity>) Query).ThenBy(keySelector));
        }

        public IQuery<TEntity> ThenByDescending<TKey>(System.Linq.Expressions.Expression<Func<TEntity, TKey>> keySelector)
        {
            return new RepositoryOrderedQuery<TEntity>(((IOrderedQueryable<TEntity>)Query).ThenByDescending(keySelector));
        }
    }
}
