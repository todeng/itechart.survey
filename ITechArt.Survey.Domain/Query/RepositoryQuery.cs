﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using ITechArt.Survey.Domain.Interfaces;
using ITechArt.Survey.Infra.Data.Extensions;
using Microsoft.EntityFrameworkCore;

namespace ITechArt.Survey.Infra.Data.Query
{
    public class RepositoryQuery<TEntity> : IQuery<TEntity> where TEntity : class
    {
        protected IQueryable<TEntity> Query { get; }

        public RepositoryQuery(IQueryable<TEntity> query)
        {
            Query = query;
        }

        public IQuery<TEntity> Include(Expression<Func<TEntity, object>> include)
        {
            return new RepositoryQuery<TEntity>(Query.IncludeMultiple(include));
        }

        public IOrderedQuery<TEntity> OrderBy<TKey>(Expression<Func<TEntity, TKey>> keySelector)
        {
            return new RepositoryOrderedQuery<TEntity>(Query.OrderBy(keySelector));
        }

        public IOrderedQuery<TEntity> OrderByDescending<TKey>(Expression<Func<TEntity, TKey>> keySelector)
        {
            return new RepositoryOrderedQuery<TEntity>(Query.OrderByDescending(keySelector));
        }

        public IQuery<TResult> Select<TResult>(Expression<Func<TEntity, TResult>> selector)
            where TResult : class
        {
            return new RepositoryQuery<TResult>(Query.Select(selector));
        }

        public IQuery<TEntity> Where(Expression<Func<TEntity, bool>> predicate)
        {
            return new RepositoryQuery<TEntity>(Query.Where(predicate));
        }

        public IQuery<TEntity> Take(int count)
        {
            return new RepositoryQuery<TEntity>(Query.Take(count));
        }

        public IQuery<TEntity> Skip(int count)
        {
            return new RepositoryQuery<TEntity>(Query.Skip(count));
        }

        public int Count()
        {
            return Query.Count();
        }

        public async Task<int> CountAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            return await Query.CountAsync(cancellationToken);
        }

        public TEntity FirstOrDefault()
        {
            return Query.FirstOrDefault();
        }

        public async Task<TEntity> FirstOrDefaultAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            return await Query.FirstOrDefaultAsync(cancellationToken);
        }

        public IEnumerable<TEntity> ToList()
        {
            return Query.ToList();
        }

        public async Task<IEnumerable<TEntity>> ToListAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            return await Query.ToListAsync(cancellationToken);
        }
    }
}
