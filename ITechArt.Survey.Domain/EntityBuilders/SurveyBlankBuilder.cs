﻿using ITechArt.Survey.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace ITechArt.Survey.Infra.Data.EntityBuilders
{
    public class SurveyBlankBuilder: IEntityBuilder
    {
        public void Build(ModelBuilder builder)
        {
            builder.Entity<SurveyBlank>();
        }
    }
}
