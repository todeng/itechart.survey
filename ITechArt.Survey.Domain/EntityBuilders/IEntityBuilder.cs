﻿using Microsoft.EntityFrameworkCore;

namespace ITechArt.Survey.Infra.Data.EntityBuilders
{
    public interface IEntityBuilder
    {
        void Build(ModelBuilder builder);
    }
}
