﻿using ITechArt.Survey.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace ITechArt.Survey.Infra.Data.EntityBuilders
{
    public class QuestionAnswerBuilder: IEntityBuilder
    {
        public void Build(ModelBuilder builder)
        {
            builder.Entity<QuestionAnswer>();
        }
    }
}
