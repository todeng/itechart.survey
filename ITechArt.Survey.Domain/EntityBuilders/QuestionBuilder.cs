﻿using ITechArt.Survey.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace ITechArt.Survey.Infra.Data.EntityBuilders
{
    public class QuestionBuilder: IEntityBuilder
    {
        public void Build(ModelBuilder builder)
        {
            builder.Entity<QuestionAnswer>()
                .HasOne(b => b.Question)
                .WithMany(a => a.Answers)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
