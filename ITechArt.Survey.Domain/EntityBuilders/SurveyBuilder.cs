﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace ITechArt.Survey.Infra.Data.EntityBuilders
{
    public class SurveyBuilder : IEntityBuilder
    {
        public void Build(ModelBuilder builder)
        {
            builder.Entity<Survey.Domain.Models.Survey>();
        }
    }
}
