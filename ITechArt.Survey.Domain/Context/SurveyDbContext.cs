﻿using System;
using System.Linq;
using ITechArt.Survey.Domain.Models;
using ITechArt.Survey.Infra.Data.EntityBuilders;
using ITechArt.Survey.Infra.Data.Extensions;
using Microsoft.EntityFrameworkCore;

namespace ITechArt.Survey.Infra.Data.Context
{
    public class SurveyDbContext : DbContext
    {
        public SurveyDbContext(DbContextOptions<SurveyDbContext> options)
            :base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            this.GetType().Assembly.GetTypes()
                .Where(type => type.GetInterfaces().Contains(typeof(IEntityBuilder)))
                .ToList()
                .ForEach(type =>
                {
                    var builder = Activator.CreateInstance(type) as IEntityBuilder;
                    builder?.Build(modelBuilder);
                });
        }
    }
}
