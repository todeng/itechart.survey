﻿using ITechArt.Survey.Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;

namespace ITechArt.Survey.Infra.Data.Transaction
{
    public class RepositoryTransaction : ITransaction
    {
        private readonly IDbContextTransaction _transaction;

        public RepositoryTransaction(DbContext context)
        {
            _transaction = context.Database.BeginTransaction();
        }

        public void Commit()
        {
            _transaction.Commit();
        }

        public void Rollback()
        {
            _transaction.Rollback();
        }

        public void Dispose()
        {
            _transaction.Dispose();
        }
    }
}
