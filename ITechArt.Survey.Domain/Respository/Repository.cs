﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using ITechArt.Survey.Domain.Interfaces;
using ITechArt.Survey.Infra.Data.Extensions;
using ITechArt.Survey.Infra.Data.Query;
using Microsoft.EntityFrameworkCore;

namespace ITechArt.Survey.Infra.Data.Respository
{
    public class Repository<TEntity, TPrimaryKey> : IRepository<TEntity, TPrimaryKey>
        where TEntity : class, IBaseEntity<TPrimaryKey>
    {
        private readonly DbContext _context;
        private readonly DbSet<TEntity> _dbSet;

        public Repository(DbContext context)
        {
            _context = context;
            _dbSet = context.Set<TEntity>();
        }

        public TEntity GetById(TPrimaryKey id)
        {
            return _dbSet.Find(id);
        }

        public async Task<TEntity> GetByIdAsync(TPrimaryKey id, CancellationToken cancellationToken = default(CancellationToken))
        {
            return await _dbSet.FindAsync(id);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _dbSet.ToList();
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            return await _dbSet.ToListAsync(cancellationToken);
        }

        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbSet.Where(predicate).ToList();
        }

        public async Task<IEnumerable<TEntity>> FindAsync(Expression<Func<TEntity, bool>> predicate,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            return await _dbSet.Where(predicate).ToListAsync(cancellationToken);
        }

        public TEntity Add(TEntity obj)
        {
            _dbSet.Add(obj);
            return obj;
        }

        public IEnumerable<TEntity> AddRange(IEnumerable<TEntity> entities)
        {
            _dbSet.AddRange(entities);
            return entities;
        }

        public TEntity Update(TEntity obj)
        {
            return _context.UpdateEntity<TEntity, TPrimaryKey>(obj);
        }

        public IEnumerable<TEntity> UpdateRange(IEnumerable<TEntity> objs)
        {
            return objs.Select(Update);
        }

        public TEntity UpdateGraph(TEntity targetEntitie, TEntity sourceEntity)
        {
            return _context.UpdateGraphEntity<TEntity, TPrimaryKey>(targetEntitie, sourceEntity);
        }

        public void Remove(TEntity obj)
        {
            _dbSet.Remove(obj);
        }

        public void Remove(Expression<Func<TEntity, bool>> predicate)
        {
            _dbSet.RemoveRange(_dbSet.Where(predicate));
        }

        public void RemoveRange(IEnumerable<TEntity> objs)
        {
            _dbSet.RemoveRange(objs);
        }

        public int Count()
        {
            return _dbSet.Count();
        }

        public int Count(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbSet.Count(predicate);
        }

        public Task<int> CountAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            return _dbSet.CountAsync(cancellationToken);
        }

        public Task<int> CountAsync(Expression<Func<TEntity, bool>> predicate,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            return _dbSet.CountAsync(predicate, cancellationToken);
        }

        public virtual IQuery<TEntity> GetQuery()
        {
            return new RepositoryQuery<TEntity>(_dbSet);
        }
    }
}
