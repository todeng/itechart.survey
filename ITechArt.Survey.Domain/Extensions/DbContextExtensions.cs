﻿using System;
using System.Collections.Generic;
using ITechArt.Survey.Domain.Interfaces;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace ITechArt.Survey.Infra.Data.Extensions
{
    public static class DbContextExtensions
    {
        public static ICollection<TEntity> UpdateGraphEntityCollection<TEntity, TPrimaryKey>(this DbContext context, ICollection<TEntity> targetEntities, ICollection<TEntity> sourceEntities)
            where TEntity : class, IBaseEntity<TPrimaryKey>
        {
            targetEntities = context.DeleteRedundantEntities<TEntity, TPrimaryKey>(targetEntities, sourceEntities);
            targetEntities = context.CreateOrUpdateGrathEntitie<TEntity, TPrimaryKey>(targetEntities, sourceEntities);

            return targetEntities;
        }

        public static ICollection<TEntity> CreateOrUpdateGrathEntitie<TEntity, TPrimaryKey>(this DbContext context, ICollection<TEntity> targetEntities, ICollection<TEntity> sourceEntities)
            where TEntity : class, IBaseEntity<TPrimaryKey>
        {
            var entitySet = context.Set<TEntity>();

            foreach (var sourceEntitie in sourceEntities)
            {
                if (sourceEntitie.Id.Equals(default(TPrimaryKey)))
                {
                    entitySet.Add(sourceEntitie);
                    targetEntities.Add(sourceEntitie);
                }
                else
                {
                    var targetEntitie = targetEntities.FirstOrDefault(x => x.Id.Equals(sourceEntitie.Id));

                    if (targetEntitie != null)
                    {
                        context.UpdateGraphEntity<TEntity, TPrimaryKey>(targetEntitie, sourceEntitie);
                    }
                }
            }

            return targetEntities;
        }

        public static ICollection<TEntity> DeleteRedundantEntities<TEntity, TPrimaryKey>(this DbContext context, ICollection<TEntity> targetEntities, ICollection<TEntity> sourceEntities)
            where TEntity : class, IBaseEntity<TPrimaryKey>
        {
            var entitySet = context.Set<TEntity>();
            var savedEntities = new List<TEntity>();

            foreach (var entitie in targetEntities)
            {
                if (sourceEntities.Any(src => src.Id.Equals(entitie.Id)))
                {
                    savedEntities.Add(entitie);
                }
                else
                {
                    entitySet.Remove(entitie);
                }
            }

            return savedEntities;
        }

        public static  TEntity UpdateGraphEntity<TEntity, TPrimaryKey>(this DbContext context, TEntity targetEntity, TEntity sourceEntity)
            where TEntity : class, IBaseEntity<TPrimaryKey>
        {
            var entityType = typeof(TEntity);
            var entityProperties = entityType.GetProperties().Where(prop => prop.CanRead && prop.CanWrite);
            var hasChanges = false;

            foreach (var entityProperty in entityProperties)
            {
                var value = entityProperty.GetValue(sourceEntity, null);

                if (value == null) continue;

                if (
                    entityProperty.PropertyType.IsAssignableToGenericType(typeof(ICollection<>)))
                {
                    var entity = entityProperty.PropertyType.GenericTypeArguments[0];
                    var baseInterface = entity.GetAssignableToGenericType(typeof(IBaseEntity<>));

                    if (baseInterface == default(Type)) continue;
                    var primaryKeyType = baseInterface.GenericTypeArguments[0];

                    var result = typeof(DbContextExtensions)
                        .GetMethod("UpdateGraphEntityCollection")
                        .MakeGenericMethod(entity, primaryKeyType)
                        .Invoke(null, new[] { context, entityProperty.GetValue(targetEntity), value });

                    entityProperty.SetValue(targetEntity, result);
                    hasChanges = true;
                }
                else
                {
                    var targetValue = entityProperty.GetValue(targetEntity);

                    if (targetValue == null || targetValue.Equals(value)) continue;

                    entityProperty.SetValue(targetEntity, value);
                    hasChanges = true;
                }
            }

            return hasChanges ? context.UpdateEntity<TEntity, TPrimaryKey>(targetEntity) : targetEntity;
        }

        public static TEntity UpdateEntity<TEntity, TPrimaryKey>(this DbContext context, TEntity entity)
            where TEntity : class, IBaseEntity<TPrimaryKey>
        {
            var entry = context.Entry(entity);

            if (entry.State == EntityState.Detached)
            {
                context.Attach(entity);
            }
            entry.State = EntityState.Modified;

            return entity;
        }
    }
}
