﻿using System;

namespace ITechArt.Survey.Infra.Data.Extensions
{
    public static class ReflectionExtensions
    {
        public static bool IsAssignableToGenericType(this Type type, Type genericType)
        {
            return GetAssignableToGenericType(type, genericType) != default(Type);
        }

        public static Type GetAssignableToGenericType(this Type type, Type genericType)
        {
            var interfaceTypes = type.GetInterfaces();

            if (type.IsGenericType && type.GetGenericTypeDefinition() == genericType)
                return type;

            foreach (var it in interfaceTypes)
            {
                if (it.IsGenericType && it.GetGenericTypeDefinition() == genericType)
                    return it;
            }

            var baseType = type.BaseType;
            return baseType != null ? GetAssignableToGenericType(baseType, genericType) : default(Type);
        }
    }
}
