﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;

namespace ITechArt.Survey.Infra.Data.Extensions
{
    public static class QueryableExtensions
    {
        public static IQueryable<T> IncludeMultiple<T>(this IQueryable<T> query, params Expression<Func<T, object>>[] includePaths)
            where T : class
        {
            if (includePaths.Any())
            {
                query = includePaths.Aggregate(query, (current, include) => current.Include(include.GetPropertyPath()));
            }

            return query;
        }
    }
}
