﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace ITechArt.Survey.Infra.Data.Extensions
{
    public static class ExpressionExtensions
    {
        public static string LambdaExpressionToPropertyPath(this Expression expression)
        {
            if (expression.NodeType == ExpressionType.Lambda)
            {
                var lambdaExpression = expression as LambdaExpression;

                return LambdaExpressionToPropertyPath(lambdaExpression.Body);
            }

            if (expression.NodeType == ExpressionType.Call)
            {
                var methodCallExpression = expression as MethodCallExpression;

                if (methodCallExpression.Arguments.Count > 0)
                {
                    return string.Join(".", methodCallExpression.Arguments.Select(LambdaExpressionToPropertyPath));
                }
            }

            if (expression.NodeType == ExpressionType.MemberAccess)
            {
                var memberExpression = expression as MemberExpression;

                return memberExpression.Member.Name;
            }

            return string.Empty;
        }

        public static string GetPropertyPath<T>(this Expression<Func<T, object>> expression)
        {
            return LambdaExpressionToPropertyPath(expression);
        }
    }
}
