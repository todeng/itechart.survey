﻿using ITechArt.Survey.Domain.Interfaces;
using ITechArt.Survey.Infra.Data.UoW;
using Microsoft.Extensions.DependencyInjection;

namespace ITechArt.Survey.Infra.Data.Extensions
{
    public static class DataServiceCollectionExtensions
    {
        public static IServiceCollection AddApplicationData(this IServiceCollection services)
        {
            services.AddTransient<IUnitOfWork, UnitOfWork>();
            return services;
        }
    }
}
