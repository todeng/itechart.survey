﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using ITechArt.Survey.Domain.Interfaces;
using ITechArt.Survey.Domain.Models;
using ITechArt.Survey.Infra.Data.Respository;
using ITechArt.Survey.Infra.Data.Transaction;
using Microsoft.EntityFrameworkCore;

namespace ITechArt.Survey.Infra.Data.UoW
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DbContext _context;
        private readonly Dictionary<Type, object> _repositories;

        public UnitOfWork(DbContext context)
        {
            _context = context;
            _repositories = new Dictionary<Type, object>();
        }

        public IRepository<User, string> Users => GetRepository<User, string>();
        public IRepository<Domain.Models.Survey, int> Surveys => GetRepository<Domain.Models.Survey, int>();
        public IRepository<QuestionVariant, int> QuestionVariants => GetRepository<QuestionVariant, int>();
        public IRepository<QuestionAnswer, int> QuestionAnswers => GetRepository<QuestionAnswer, int>();
        public IRepository<Question, int> Questions => GetRepository<Question, int>();
        public IRepository<Comment, int> Comments => GetRepository<Comment, int>();
        public IRepository<SurveyBlank, int> SurveyBlanks => GetRepository<SurveyBlank, int>();


        public IRepository<TEntity, TKey> GetRepository<TEntity, TKey>()
            where TEntity : class, IBaseEntity<TKey>
        {
            if (_repositories.TryGetValue(typeof(TEntity), out object repository))
            {
                return repository as IRepository<TEntity, TKey>;
            }

            var newRepository = new Repository<TEntity, TKey>(_context);

            _repositories.Add(typeof(TEntity), newRepository);

            return newRepository;
        }

        public int Save()
        {
            return _context.SaveChanges();
        }

        public async Task<int> SaveAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }

        public ITransaction BeginTrainsaction()
        {
            return new RepositoryTransaction(_context);
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
