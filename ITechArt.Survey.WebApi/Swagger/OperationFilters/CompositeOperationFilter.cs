﻿using System.Collections.Generic;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace ITechArt.Survey.WebApi.Swagger.OperationFilters
{
    public class CompositeOperationFilter: IOperationFilter
    {
        private readonly List<IOperationFilter> _filters = new List<IOperationFilter>()
        {
            new OperationsWithAuthorizationFilter()
        };

        public void Apply(Operation operation, OperationFilterContext context)
        {
            foreach (var filter in _filters)
            {
                filter.Apply(operation, context);
            }
        }
    }
}
