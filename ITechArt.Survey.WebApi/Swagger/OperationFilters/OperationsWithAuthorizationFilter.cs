﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace ITechArt.Survey.WebApi.Swagger.OperationFilters
{
    public class OperationsWithAuthorizationFilter : IOperationFilter
    {

        public void Apply(Operation operation, OperationFilterContext context)
        {
            var authAttributes = context.ApiDescription
                .ControllerAttributes()
                .Union(context.ApiDescription.ActionAttributes())
                .OfType<AuthorizeAttribute>();

            if (authAttributes.Any())
            {
                operation.Parameters = new List<IParameter>()
                {
                    new NonBodyParameter()
                    {
                        Name = "Authorization",
                        In = "header",
                        Type = "string",
                        Required = true,
                        Description = "Bearer"
                    }
                };

                operation.Responses.Add("401", new Response { Description = "Unauthorized" });
            }
        }
    }
}
