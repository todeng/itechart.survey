﻿using System.Collections.Generic;
using ITechArt.Survey.Infra.Services.Models;
using ITechArt.Survey.WebApi.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace ITechArt.Survey.WebApi.Controllers
{
    public class BaseController : Controller
    {
        [NonAction]
        public IActionResult SearchResult<T>(SearchResultServiceModel<T> searchResult)
        {
            return Json(searchResult);
        }
    }
}
