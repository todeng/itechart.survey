using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using ITechArt.Survey.Infra.Services.Interfaces;
using ITechArt.Survey.Infra.Services.SearchCriterias;
using ITechArt.Survey.WebApi.ViewModels;
using ITechArt.Survey.WebApi.ViewModels.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ITechArt.Survey.WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/users")]
    public class UserController : BaseController
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [Authorize]
        [HttpGet("{id}")]
        public async Task<IActionResult> GetUserDetails(string id)
        {
            var user = await _userService.GetUserByIdAsync(id);

            if (user == null) return NotFound();

            return Json(Mapper.Map<UserViewModel>(user));
        }


        [HttpGet("{id}/photo")]
        public async Task<IActionResult> GetUserPhoto(string id)
        {
            var user = await _userService.GetUserByIdAsync(id);

            if (user == null || string.IsNullOrEmpty(user.Photo)) return NotFound();

            return Redirect(user.Photo);
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> GetUsers(SearchCriteriaViewModel searchCriteria)
        {
            var searchResult = await _userService.SearchUsersAsync(Mapper.Map<SearchCriteria>(searchCriteria));

            return SearchResult(searchResult);
        }

        [Authorize]
        [HttpGet("department/{departmentId}")]
        public async Task<IActionResult> GetUsersByDepartmentId(int departmentId)
        {
            var users = await _userService.GetUsersByDepartmentIdAsync(departmentId);

            return Json(users);
        }
    }
}