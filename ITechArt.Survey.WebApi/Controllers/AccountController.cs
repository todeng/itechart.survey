﻿using System.IdentityModel.Tokens.Jwt;
using System.Threading.Tasks;
using AutoMapper;
using ITechArt.Survey.Infra.Identity.Providers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ITechArt.Survey.Infra.Services.Interfaces;
using ITechArt.Survey.WebApi.ViewModels.Account;
using ITechArt.Survey.WebApi.ViewModels.Users;

namespace ITechArt.Survey.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class AccountController : BaseController
    {
        private readonly IUserService _userService;
        private readonly ITokenProvider _tokenProvider;

        public AccountController(IUserService userService, ITokenProvider tokenProvider)
        {
            _userService = userService;
            _tokenProvider = tokenProvider;
        }

        [Authorize]
        [HttpGet("me")]
        public async Task<ActionResult> Me()
        {
            var username = User.Identity.Name;
            var user = await _userService.GetUserByUsernameAsync(username);

            return Json(Mapper.Map<UserViewModel>(user));
        }

        [HttpPost("token")]
        public async Task<ActionResult> Token([FromBody]TokenRequestViewModel data)
        {
            var user = await _userService.GetUserByUsernameAndPasswordAsync(data.Username, data.Password);

            if (user == null) return BadRequest();

            var token = _tokenProvider.CreateToken(user);

            return Json(new
            {
                accessToken = new JwtSecurityTokenHandler().WriteToken(token),
                userData = Mapper.Map<UserViewModel>(user),
                validFrom = token.ValidFrom,
                validTo = token.ValidTo
            });
        }
    }
}
