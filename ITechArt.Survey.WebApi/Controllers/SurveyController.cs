using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using ITechArt.Survey.Domain.Models;
using ITechArt.Survey.Infra.Identity.Extensions;
using ITechArt.Survey.Infra.Services.Interfaces;
using ITechArt.Survey.Infra.Services.Models;
using ITechArt.Survey.Infra.Services.SearchCriterias;
using ITechArt.Survey.WebApi.Filters;
using ITechArt.Survey.WebApi.ViewModels;
using ITechArt.Survey.WebApi.ViewModels.Survey;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ITechArt.Survey.WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Survey")]
    public class SurveyController : BaseController
    {
        private readonly ISurveyService _surveyService;

        public SurveyController(ISurveyService surveyService, IUserService userService)
        {
            _surveyService = surveyService;
        }

        [HttpPost]
        [Authorize]
        [ModelStateValidationActionFilter]
        public async Task<IActionResult> Create([FromBody] SurveyViewModel survey)
        {
            var newSurvey = await _surveyService.CreateSurveyAsync(Mapper.Map<SurveyServiceModel>(survey), User.GetUserId());

            return Json(newSurvey);
        }

        [HttpPut("{id}")]
        [Authorize]
        [ModelStateValidationActionFilter]
        public async Task<IActionResult> Update([FromBody] SurveyViewModel survey)
        {
            var updatedSurvey = await _surveyService.UpdateSurveyAsync(Mapper.Map<SurveyServiceModel>(survey));

            return Json(updatedSurvey);
        }

        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Get([FromQuery] SearchCriteriaViewModel criteria)
        {
            var searchResult = await _surveyService.SearchSurveysAsync(Mapper.Map<SearchCriteria>(criteria));

            return SearchResult(searchResult);
        }

        [HttpGet("active")]
        [Authorize]
        public async Task<IActionResult> GetActive([FromQuery] SearchCriteriaViewModel criteria)
        {
            var searchResult = await _surveyService.SearchActiveSurveysAsync(Mapper.Map<SearchCriteria>(criteria));

            return SearchResult(searchResult);
        }

        [HttpGet("inactive")]
        [Authorize]
        public async Task<IActionResult> GetInactive([FromQuery] SearchCriteriaViewModel criteria)
        {
            var searchResult = await _surveyService.SearchInactiveSurveysAsync(Mapper.Map<SearchCriteria>(criteria));

            return SearchResult(searchResult);
        }

        [HttpGet("{id}")]
        [Authorize]
        public async Task<IActionResult> GetDetail(int id)
        {
            var survey = await _surveyService.GetSurveyDetailsAndAddViewAsync(id);

            if (survey == null) return NotFound();

            return Json(survey);
        }

        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult> DeleteSurvey(int id)
        {
            var survey = await _surveyService.GetSurveyDetailsAsync(id);

            if (survey == null) return NotFound();

            await _surveyService.DeleteSurveyAsync(id);

            return Json(survey);
        }

        [HttpGet("{id}/report")]
        [Authorize]
        public async Task<IActionResult> GetSurveyReportData(int id)
        {
            var surveyResult = await _surveyService.GetSurveyReportDataAsync(id);

            return Json(surveyResult);
        }

        [HttpGet("{id}/comments")]
        [Authorize]
        public async Task<IActionResult> GetComments(int id)
        {
            var comments = await _surveyService.GetCommentsAsync(id);

            return Json(comments);
        }

        [HttpPost("{id}/comments")]
        [Authorize]
        public async Task<IActionResult> PostComments(int id, [FromBody] CommentViewModel comment)
        {
            var postedComment = await _surveyService.AddCommentAsync(comment.Body, id, User.GetUserId());

            return Json(postedComment);
        }

        [HttpGet("{id}/myanswers")]
        [Authorize]
        public async Task<IActionResult> GetMyAnswers(int id)
        {
            var userId = User.GetUserId();
            var blank = await _surveyService.GetSurveyBlankAsync(id, userId);

            if (blank == null) return NotFound();

            return Json(blank);
        }

        [HttpPost("{id}/myanswers")]
        [Authorize]
        public async Task<IActionResult> PostMyAnswers(SurveyBlankViewModel surveyBlank)
        {
            var userId = User.GetUserId();
            var blank = await _surveyService.AddSurveyAnswersAsync(surveyBlank.SurveyId, userId, Mapper.Map<List<QuestionAnswer>>(surveyBlank.Answers));

            return Json(blank);
        }
    }
}