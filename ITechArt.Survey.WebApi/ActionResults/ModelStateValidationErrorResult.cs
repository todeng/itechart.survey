﻿using System;
using System.Linq;
using System.Net;
using ITechArt.Survey.WebApi.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace ITechArt.Survey.WebApi.ActionResults
{
    public class ModelStateValidationErrorResult: ContentResult
    {
        public ModelStateValidationErrorResult(ModelStateDictionary modelState)
        {
            Content = modelState.ToDictionary(
                kvp => string.Join(".", kvp.Key.Split('.').Select(x => Char.ToLowerInvariant(x[0]) + x.Substring(1))) ,
                kvp => kvp.Value.Errors.Select(e => e.ErrorMessage).ToArray()
            )
                .Where(x => x.Value != null && x.Value.Any())
                .ToDictionary(x => x.Key, x => x.Value)
                .SerializeToJson();

            StatusCode = (int)HttpStatusCode.BadRequest;
            ContentType = "application/json; charset=utf-8";
        }
    }
}
