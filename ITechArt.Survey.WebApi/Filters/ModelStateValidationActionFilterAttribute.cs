﻿using ITechArt.Survey.WebApi.ActionResults;
using Microsoft.AspNetCore.Mvc.Filters;

namespace ITechArt.Survey.WebApi.Filters
{
    public class ModelStateValidationActionFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var modelState = context.ModelState;

            if (!modelState.IsValid)
                context.Result = new ModelStateValidationErrorResult(modelState);

            base.OnActionExecuting(context);
        }
    }
}
