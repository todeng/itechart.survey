﻿using AutoMapper;
using FluentValidation.AspNetCore;
using ITechArt.SmgApi.Configuration;
using ITechArt.Survey.Infra.Data.Context;
using ITechArt.Survey.Infra.Data.Extensions;
using ITechArt.Survey.Infra.Identity.Extensions;
using ITechArt.Survey.Infra.Services.Extensions;
using ITechArt.Survey.Infra.Services.Mappings;
using ITechArt.Survey.WebApi.Mappings;
using ITechArt.Survey.WebApi.Swagger.OperationFilters;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Swagger;

namespace ITechArt.Survey.WebApi
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

            Mapper.Initialize(x =>
            {
                AutoMapperConfiguration.ConfigAction.Invoke(x);
                AutoMapperServiceConfiguration.ConfigAction.Invoke(x);
            });
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(Configuration);

            services.AddDbContext<SurveyDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection"),
                    x => x.MigrationsAssembly(typeof(Startup).Assembly.GetName().Name))
                );
            services.AddTransient<DbContext, SurveyDbContext>();

            services.AddOptions();
            services.Configure<SmgOptions>(Configuration.GetSection("Smg"));

            services.AddAutoMapper();
            services.AddApplicationData();
            services.AddApplicationServices();
            services.AddJwtAuthentication(Configuration);

            services.AddMvc()
                .AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<Startup>());

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "My API", Version = "v1" });
                c.OperationFilter<CompositeOperationFilter>();
            });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseAuthentication();
            app.UseMvc();
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });
        }
    }
}
