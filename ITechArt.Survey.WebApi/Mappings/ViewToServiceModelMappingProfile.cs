﻿using AutoMapper;
using ITechArt.Survey.Domain.Models;
using ITechArt.Survey.Infra.Services.Models;
using ITechArt.Survey.Infra.Services.SearchCriterias;
using ITechArt.Survey.WebApi.ViewModels;
using ITechArt.Survey.WebApi.ViewModels.Survey;

namespace ITechArt.Survey.WebApi.Mappings
{
    public class ViewToServiceModelMappingProfile : Profile
    {
        public ViewToServiceModelMappingProfile()
        {
            CreateMap<SearchCriteriaViewModel, SearchCriteria>();
            CreateMap<SurveyViewModel, SurveyServiceModel>();
            CreateMap<QuestionViewModel, QuestionServiceModel>();
            CreateMap<QuestionVariantViewModel, QuestionVariantServiceModel>();
        }

        public override string ProfileName => "ViewToServiceModelMappingProfile";
    }
}
