﻿using System;
using AutoMapper;

namespace ITechArt.Survey.WebApi.Mappings
{
    public class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(ConfigAction);
        }

        public static Action<IMapperConfigurationExpression> ConfigAction = x =>
        {
            x.AddProfile<DomainToViewModelMappingProfile>();
            x.AddProfile<ViewModelToDomainMappingProfile>();
            x.AddProfile<ServiceToViewModelMappingProfile>();
            x.AddProfile<ViewToServiceModelMappingProfile>();
        };
    }
}
