﻿using AutoMapper;
using ITechArt.Survey.Infra.Services.Models;
using ITechArt.Survey.WebApi.ViewModels;
using ITechArt.Survey.WebApi.ViewModels.Survey;

namespace ITechArt.Survey.WebApi.Mappings
{
    public class ServiceToViewModelMappingProfile : Profile
    {
        public ServiceToViewModelMappingProfile()
        {

        }

        public override string ProfileName => "ServiceToViewModelMappings";
    }
}
