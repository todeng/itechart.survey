﻿using AutoMapper;
using ITechArt.Survey.Domain.Models;
using ITechArt.Survey.WebApi.ViewModels.Survey;
using ITechArt.Survey.WebApi.ViewModels.Users;

namespace ITechArt.Survey.WebApi.Mappings
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public ViewModelToDomainMappingProfile()
        {
            CreateMap<UserViewModel, User>();
            CreateMap<SurveyViewModel, Domain.Models.Survey>();
            CreateMap<QuestionViewModel, Question>();
            CreateMap<QuestionVariantViewModel, QuestionVariant>();
            CreateMap<QuestionAnswerViewModel, QuestionAnswer>();
        }

        public override string ProfileName => "ViewModelToDomainMappings";
    }
}
