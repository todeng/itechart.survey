﻿using AutoMapper;
using ITechArt.Survey.Domain.Models;
using ITechArt.Survey.WebApi.ViewModels.Survey;
using ITechArt.Survey.WebApi.ViewModels.Users;

namespace ITechArt.Survey.WebApi.Mappings
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            CreateMap<User, UserViewModel>();
            CreateMap<Domain.Models.Survey, SurveyViewModel>();
            CreateMap<Question, QuestionViewModel>();
            CreateMap<QuestionVariant, QuestionVariantViewModel>();
            CreateMap<QuestionAnswer, QuestionAnswerViewModel>();
        }

        public override string ProfileName => "DomainToViewModelMappings";
    }
}
