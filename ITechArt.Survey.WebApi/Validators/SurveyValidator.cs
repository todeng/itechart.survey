﻿using System;
using FluentValidation;
using ITechArt.Survey.WebApi.ViewModels;
using ITechArt.Survey.WebApi.ViewModels.Survey;

namespace ITechArt.Survey.WebApi.Validators
{
    public class SurveyValidator: AbstractValidator<SurveyViewModel>
    {
        public SurveyValidator(IValidator<QuestionViewModel> questionValidator)
        {
            RuleFor(x => x.Title).NotEmpty();
            RuleFor(x => x.Content).NotEmpty();
            RuleFor(x => x.StartAt).GreaterThanOrEqualTo(DateTime.Now.AddDays(-1)).When(x => x.StartAt.HasValue);
            RuleFor(x => x.EndAt).GreaterThanOrEqualTo(x => x.StartAt).When(x => x.StartAt.HasValue);
            RuleFor(x => x.Questions).NotEmpty();
            RuleFor(x => x.Questions).SetCollectionValidator(questionValidator);
        }
    }
}
