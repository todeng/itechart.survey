﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;
using ITechArt.Survey.Infra.Services.Interfaces;
using ITechArt.Survey.WebApi.ViewModels;
using System.Threading;
using ITechArt.Survey.WebApi.ViewModels.Survey;

namespace ITechArt.Survey.WebApi.Validators
{
    public class SurveyBlankValidator: AbstractValidator<SurveyBlankViewModel>
    {
        private readonly IQuestionService _questionService;

        public SurveyBlankValidator(IQuestionService questionService)
        {
            _questionService = questionService;

            RuleFor(x => x.SurveyId).GreaterThan(0);
            RuleFor(x => x.Answers).MustAsync(ContainAllRequiredAnswersAsync);
        }

        private async Task<bool> ContainAllRequiredAnswersAsync(SurveyBlankViewModel model, IEnumerable<QuestionAnswerViewModel> anwers, CancellationToken cancellationToken)
        {
            var surveyQuestions = await _questionService.GetQuestionsBySurveyIdAsync(model.SurveyId);
            var requiredQuestionIds = surveyQuestions.Where(x => x.IsRequired).Select(x => x.Id);
            var questionWithAnswerIds = anwers.Where(x => !string.IsNullOrEmpty(x.Value)).Select(x => x.QuestionId);

            return questionWithAnswerIds.Intersect(requiredQuestionIds).Count() == requiredQuestionIds.Count();
        }
    }
}
