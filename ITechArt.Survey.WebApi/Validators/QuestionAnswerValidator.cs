﻿using System.Collections.Generic;
using FluentValidation;
using ITechArt.Survey.Infra.Services.Interfaces;
using ITechArt.Survey.WebApi.ViewModels;
using ITechArt.Survey.WebApi.ViewModels.Survey;

namespace ITechArt.Survey.WebApi.Validators
{
    public class QuestionAnswerValidator: AbstractValidator<IEnumerable<QuestionAnswerViewModel>>
    {
        private readonly IQuestionService _questionService;

        public QuestionAnswerValidator(IQuestionService questionService)
        {
            _questionService = questionService;

        }
    }
}
