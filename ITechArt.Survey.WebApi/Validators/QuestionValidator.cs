﻿using System.Linq;
using FluentValidation;
using ITechArt.Survey.Domain.Enums;
using ITechArt.Survey.WebApi.ViewModels;
using ITechArt.Survey.WebApi.ViewModels.Survey;

namespace ITechArt.Survey.WebApi.Validators
{
    public class QuestionValidator: AbstractValidator<QuestionViewModel>
    {
        public QuestionValidator(IValidator<QuestionVariantViewModel> questionVariantValidator)
        {
            RuleFor(x => x.Text).NotEmpty();
            RuleFor(x => x.Type).NotEqual(QuestionType.Empty);
            When(x => x.Type == QuestionType.RadioButtonList || x.Type == QuestionType.CheckBoxList, () =>
            {
                RuleFor(x => x.Variants).Must(x => x != null && x.Count() >= 2).SetCollectionValidator(questionVariantValidator);
            });
        }
    }
}
