﻿using FluentValidation;
using ITechArt.Survey.WebApi.ViewModels;
using ITechArt.Survey.WebApi.ViewModels.Survey;

namespace ITechArt.Survey.WebApi.Validators
{
    public class QuestionVariantValidator: AbstractValidator<QuestionVariantViewModel>
    {
        public QuestionVariantValidator()
        {
            RuleFor(x => x.Text).NotEmpty();
        }
    }
}
