﻿using System.ComponentModel.DataAnnotations;

namespace ITechArt.Survey.WebApi.ViewModels.Account
{
    public class TokenRequestViewModel
    {
        [Required]
        public string Username { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
