﻿namespace ITechArt.Survey.WebApi.ViewModels.Survey
{
    public class CommentViewModel
    {
        public string Body { get; set; }
    }
}
