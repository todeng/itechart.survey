﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace ITechArt.Survey.WebApi.ViewModels.Survey
{
    public class SurveyViewModel
    {
        [FromRoute(Name = "id")]
        public int? Id { get; set; }

        public string Title { get; set; }

        public string Content { get; set; }

        public ICollection<QuestionViewModel> Questions { get; set; }

        public DateTime? StartAt { get; set; }

        public DateTime? EndAt { get; set; }
    }
}
