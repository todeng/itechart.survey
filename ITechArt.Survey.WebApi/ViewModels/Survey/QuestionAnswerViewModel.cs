﻿using System;

namespace ITechArt.Survey.WebApi.ViewModels.Survey
{
    public class QuestionAnswerViewModel
    {
        public string Value { get; set; }
        public int QuestionId { get; set; }
        public string UserId { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
