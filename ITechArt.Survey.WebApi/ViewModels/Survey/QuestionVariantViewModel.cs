﻿namespace ITechArt.Survey.WebApi.ViewModels.Survey
{
    public class QuestionVariantViewModel
    {
        public int Id { get; set; }

        public string Text { get; set; }
    }
}
