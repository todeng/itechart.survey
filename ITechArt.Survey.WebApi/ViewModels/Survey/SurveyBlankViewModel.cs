﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace ITechArt.Survey.WebApi.ViewModels.Survey
{
    public class SurveyBlankViewModel
    {
        [FromRoute(Name = "id")]
        public int SurveyId { get; set; }

        [FromBody]
        public IEnumerable<QuestionAnswerViewModel> Answers { get; set; }
    }
}
