﻿using System.Collections.Generic;
using ITechArt.Survey.Domain.Enums;

namespace ITechArt.Survey.WebApi.ViewModels.Survey
{
    public class QuestionViewModel
    {
        public int Id { get; set; }

        public string Text { get; set; }

        public QuestionType Type { get; set; }

        public bool IsRequired { get; set; }

        public IEnumerable<QuestionVariantViewModel> Variants { get; set; }
    }
}
