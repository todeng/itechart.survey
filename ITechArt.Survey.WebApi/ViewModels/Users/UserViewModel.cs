﻿using System;

namespace ITechArt.Survey.WebApi.ViewModels.Users
{
    public class UserViewModel
    {
        public string Id { get; set; }
        public int ProfileId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FirstNameEng { get; set; }
        public string LastNameEng { get; set; }
        public bool IsEnabled { get; set; }
        public string Position { get; set; }
        public string Rank { get; set; }
        public string Room { get; set; }
        public int DeptId { get; set; }
        public string Photo { get; set; }
        public string MiddleName { get; set; }
        public DateTime Birthday { get; set; }
        public string Skype { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }
}
