﻿using Microsoft.AspNetCore.Mvc;

namespace ITechArt.Survey.WebApi.ViewModels
{
    public class SearchCriteriaViewModel
    {
        [FromQuery(Name = "page")]
        public int PageNumber { get; set; }

        [FromQuery(Name = "size")]
        public int? PageSize { get; set; }

        [FromQuery(Name = "order")]
        public string Order { get; set; }

        [FromQuery(Name = "find")]
        public string Find { get; set; }
    }
}
