﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace ITechArt.Survey.WebApi.Extensions
{
    public static class JsonExtensions
    {
        public static string SerializeToJson(this object obj)
        {
            return JsonConvert.SerializeObject(obj, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
        }
    }
}
