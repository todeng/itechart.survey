﻿using Microsoft.AspNetCore.Mvc;

namespace ITechArt.Survey.WebApi.Extensions
{
    public static class ControllerExtensions
    {
        public static IActionResult ValidationError(this Controller controller)
        {
            return controller.Json(controller.ModelState);
        }
    }
}
