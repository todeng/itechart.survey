﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using ITechArt.SmgApi.Models;
using ITechArt.Survey.Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ITechArt.Survey.Infra.Services.Interfaces;
using ITechArt.Survey.Domain.Models;
using ITechArt.Survey.Infra.Data.Context;
using ITechArt.Survey.Infra.Data.Extensions;
using ITechArt.Survey.Infra.Services.Extensions;
using ITechArt.SmgApi.Configuration;

namespace ITechArt.Survey.UserLoader
{
    class Program
    {
        static readonly IConfiguration Configuration = CreateConfiguration();

        static void Main(string[] args)
        {
            InitializeMappers();

            MainAsync(args).Wait();
        }

        public static async Task MainAsync(string[] args)
        {
            var serviceProvider = CreateServiceProvider();
            var smgService = serviceProvider.GetService<ISmgService>();
            var unitOfWork = serviceProvider.GetService<IUnitOfWork>();

            var sessionId = await smgService.AuthenticateAsync("dzianis.heiman", "6S5a5g3n5e9D3");

            if (sessionId > 0)
            {
                var profiles = await smgService.GetProfilesAsync(sessionId);
                unitOfWork.Users.AddRange(AutoMapper.Mapper.Map<List<User>>(profiles));
            }

            await unitOfWork.SaveAsync();
        }

        public static void InitializeMappers()
        {
            AutoMapper.Mapper.Initialize(config =>
            {
                config.CreateMap<Profile, User>()
                    .ForMember(dist => dist.Photo, y => y.MapFrom(src => src.Image))
                    .ForMember(dist => dist.Position, y => y.MapFrom(src => src.Rank));
            });
        }

        public static IServiceProvider CreateServiceProvider()
        {
            var services = new ServiceCollection();

            services.AddOptions();
            services.Configure<SmgOptions>(Configuration.GetSection("Smg"));

            services.AddDbContext<SurveyDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection"),
                    x => x.MigrationsAssembly(typeof(Program).Assembly.GetName().Name))
            );
            services.AddTransient<DbContext, SurveyDbContext>();

            services.AddApplicationData();
            services.AddApplicationServices();

            return services.BuildServiceProvider();
        }

        public static IConfiguration CreateConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();

            return builder.Build();
        }
    }
}