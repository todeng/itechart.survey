﻿namespace ITechArt.Survey.Infra.Services.Models
{
    public class QuestionVariantServiceModel
    {
        public int Id { get; set; }
        public string Text { get; set; }
    }
}
