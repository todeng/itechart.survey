﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ITechArt.Survey.Infra.Services.Models
{
    public class SurveyBlankServiceModel
    {
        public int SurveyId { get; set; }
        public virtual ICollection<QuestionAnswerServiceModel> Answers { get; set; }
        public string ParticipantId { get; set; }

        public DateTime CreatedAt { get; set; }

        public SurveyBlankServiceModel()
        {
            Answers = new List<QuestionAnswerServiceModel>();
        }
    }
}
