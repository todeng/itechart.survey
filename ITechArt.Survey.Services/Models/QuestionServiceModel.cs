﻿using System.Collections.Generic;
using ITechArt.Survey.Domain.Enums;

namespace ITechArt.Survey.Infra.Services.Models
{
    public class QuestionServiceModel
    {
        public int Id { get; set; }

        public string Text { get; set; }

        public QuestionType Type { get; set; }

        public bool IsRequired { get; set; }

        public IEnumerable<QuestionVariantServiceModel> Variants { get; set; }
    }
}
