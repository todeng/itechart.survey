﻿using System.Collections.Generic;
using ITechArt.Survey.Infra.Services.SearchCriterias;

namespace ITechArt.Survey.Infra.Services.Models
{
    public class SearchResultServiceModel<T>
    {
        public ISearchCriteria Criteria { get; set; }
        public IEnumerable<T> Result { get; set; }
        public int Count { get; set; }
    }
}
