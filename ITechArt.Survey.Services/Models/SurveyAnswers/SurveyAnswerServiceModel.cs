﻿using ITechArt.Survey.Domain.Enums;
using ITechArt.Survey.Domain.Models;

namespace ITechArt.Survey.Infra.Services.Models.SurveyAnswers
{
    public class SurveyAnswerServiceModel
    {
        public SurveyAnswerServiceModel()
        {

        }

        public SurveyAnswerServiceModel(QuestionAnswer original)
        {
            OriginalValue = original.Value;
            QuestionId = original.QuestionId;
            QuestionType = original.Question.Type;
        }

        public string OriginalValue { get; set; }
        public int QuestionId { get; set; }
        public QuestionType QuestionType { get; set; }
    }
}
