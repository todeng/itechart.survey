﻿using ITechArt.Survey.Domain.Models;
using System.Collections.Generic;
using System.Linq;

namespace ITechArt.Survey.Infra.Services.Models.SurveyAnswers
{
    public class SurveyCheckBoxListAnswerServiceModel : SurveyAnswerServiceModel
    {
        public ICollection<int> Value { get; set; }
        public IDictionary<int, string> Labels { get; set; }
    }
}
