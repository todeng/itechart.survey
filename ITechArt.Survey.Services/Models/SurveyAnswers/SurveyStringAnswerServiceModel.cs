﻿using ITechArt.Survey.Domain.Models;

namespace ITechArt.Survey.Infra.Services.Models.SurveyAnswers
{
    public class SurveyStringAnswerServiceModel: SurveyAnswerServiceModel
    {
        public string Value { get; set; }
    }
}
