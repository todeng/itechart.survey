﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ITechArt.Survey.Domain.Models;

namespace ITechArt.Survey.Infra.Services.Models.SurveyAnswers
{
    public class SurveyRadioButtonListAnswerServiceModel : SurveyAnswerServiceModel
    {
        public int Value { get; set; }
        public string Label { get; set; }
    }
}
