﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ITechArt.Survey.Infra.Services.Models
{
    public class UserShortServiceModel
    {
        public string Id { get; set; }
        public int ProfileId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsEnabled { get; set; }
        public int DeptId { get; set; }
        public string Photo { get; set; }
    }
}
