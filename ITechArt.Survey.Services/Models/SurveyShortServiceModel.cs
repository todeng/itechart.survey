﻿using System;

namespace ITechArt.Survey.Infra.Services.Models
{
    public class SurveyShortServiceModel
    {
        public int Id { get; set; }
        public bool Active { get; set; }
        public string Title { get; set; }
        public int ViewsCount { get; set; }
        public int CommentsCount { get; set; }
        public int ParticipantsCount { get; set; }
        public string CreatedBy { get; set; }
        public DateTime StartAt { get; set; }
        public DateTime EndAt { get; set; }
    }
}
