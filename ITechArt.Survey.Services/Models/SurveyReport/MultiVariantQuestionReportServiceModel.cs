﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ITechArt.Survey.Infra.Services.Models.SurveyReport
{
    public class MultiVariantQuestionReportServiceModel: QuestionReportServiceModel
    {
        public Dictionary<int, int> Stats { get; set; }
    }
}
