﻿using System.Collections.Generic;
using ITechArt.Survey.Infra.Services.Models.SurveyAnswers;

namespace ITechArt.Survey.Infra.Services.Models.SurveyReport
{
    public class QuestionReportServiceModel
    {
        public QuestionServiceModel Question { get; set; }
    }
}
