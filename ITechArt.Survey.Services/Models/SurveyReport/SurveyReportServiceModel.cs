﻿using System.Collections.Generic;

namespace ITechArt.Survey.Infra.Services.Models.SurveyReport
{
    public class SurveyReportServiceModel
    {
        public int SurveyId { get; set; }
        public IEnumerable<UserShortServiceModel> Participants { get; set; }
        public IEnumerable<QuestionReportServiceModel> QuestionsReports { get; set; }

        public SurveyReportServiceModel()
        {
            Participants = new List<UserShortServiceModel>();
            QuestionsReports = new List<QuestionReportServiceModel>();
        }
    }
}
