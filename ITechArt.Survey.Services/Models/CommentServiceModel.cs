﻿using System;

namespace ITechArt.Survey.Infra.Services.Models
{
    public class CommentServiceModel
    {
        public int Id { get; set; }
        public string Body { get; set; }
        public DateTime CreatedAt { get; set; }
        public UserShortServiceModel Author { get; set; }
        public int SurveyId { get; set; }
    }
}
