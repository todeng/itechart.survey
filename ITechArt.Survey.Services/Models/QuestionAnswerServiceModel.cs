﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ITechArt.Survey.Infra.Services.Models
{
    public class QuestionAnswerServiceModel
    {
        public string Value { get; set; }
        public int QuestionId { get; set; }
    }
}
