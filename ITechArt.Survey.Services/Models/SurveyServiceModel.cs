﻿using System;
using System.Collections.Generic;

namespace ITechArt.Survey.Infra.Services.Models
{
    public class SurveyServiceModel
    {
        public int? Id { get; set; }

        public string Title { get; set; }

        public string Content { get; set; }

        public ICollection<QuestionServiceModel> Questions { get; set; }

        public int ViewsCount { get; set; }

        public DateTime? StartAt { get; set; }

        public DateTime? EndAt { get; set; }

        public string CreatedById { get; set; }

        public DateTime? CreatedAt { get; set; }
    }
}
