﻿using System;

namespace ITechArt.Survey.Infra.Services.Extensions
{
    public static class EnumExtensions
    {
        public static TEnum ToEnum<TEnum>(this string value, TEnum defaultValue) where TEnum : struct
        {
            if (string.IsNullOrEmpty(value))
            {
                return defaultValue;
            }

            return Enum.TryParse(value, true, out TEnum result) ? result : defaultValue;
        }
    }
}
