﻿using ITechArt.Survey.Domain.Enums;

namespace ITechArt.Survey.Infra.Services.Extensions
{
    public static class QuestionTypeExtensions
    {
        public static bool IsHasVariants(this QuestionType questionType)
        {
            return questionType == QuestionType.CheckBoxList
                || questionType == QuestionType.RadioButtonList;
        }
    }
}
