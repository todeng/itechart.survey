﻿using ITechArt.Survey.Domain.Models;

namespace ITechArt.Survey.Infra.Services.Extensions
{
    public static class QuestionExtensions
    {
        public static bool IsHasVariants(this Question question)
        {
            return question.Type.IsHasVariants();
        }
    }
}
