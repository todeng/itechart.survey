﻿using ITechArt.Survey.Infra.Services.Implementations;
using ITechArt.Survey.Infra.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace ITechArt.Survey.Infra.Services.Extensions
{
    public static class ServicesServiceCollectionExtensions
    {
        public static IServiceCollection AddApplicationServices(this IServiceCollection services)
        {
            services.AddScoped<ISmgService, SmgService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IQuestionService, QuestionService>();
            services.AddScoped<ISurveyService, SurveyService>();

            return services;
        }
    }
}
