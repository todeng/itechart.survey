﻿using System;
using System.Linq;
using System.Linq.Expressions;
using ITechArt.Survey.Domain.Interfaces;
using ITechArt.Survey.Infra.Services.SearchCriterias;

namespace ITechArt.Survey.Infra.Services.Extensions
{
    public static class QueryExtensions
    {
        public static IQuery<T> ApplySearch<T>(this IQuery<T> query, ISearchCriteria criteria)
            where T : class
        {
            return criteria != null ? criteria.ApplyQuery(query) : query;
        }

        public static IQuery<T> OrderBy<T>(this IQuery<T> query, string propertyName)
        {
            return query.OrderBy(ToLambda<T>(propertyName));
        }

        public static IQuery<T> OrderByDescending<T>(this IQuery<T> query, string propertyName)
        {
            return query.OrderByDescending(ToLambda<T>(propertyName));
        }

        public static IQuery<T> ThenBy<T>(this IOrderedQuery<T> query, string propertyName)
        {
            return query.ThenBy(ToLambda<T>(propertyName));
        }

        public static IQuery<T> ThenByDescending<T>(this IOrderedQuery<T> query, string propertyName)
        {
            return query.ThenByDescending(ToLambda<T>(propertyName));
        }

        private static Expression<Func<T, object>> ToLambda<T>(string propertyName)
        {
            var parameter = Expression.Parameter(typeof(T));
            var property = Expression.Property(parameter, propertyName);
            var propAsObject = Expression.Convert(property, typeof(object));

            return Expression.Lambda<Func<T, object>>(propAsObject, parameter);
        }
    }
}
