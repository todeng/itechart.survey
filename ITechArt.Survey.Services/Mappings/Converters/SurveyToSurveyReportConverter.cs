﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using ITechArt.Survey.Infra.Services.Models;
using ITechArt.Survey.Infra.Services.Models.SurveyReport;

namespace ITechArt.Survey.Infra.Services.Mappings.Converters
{
    public class SurveyToSurveyReportConverter : ITypeConverter<Domain.Models.Survey, SurveyReportServiceModel>
    {
        public SurveyReportServiceModel Convert(Domain.Models.Survey source, SurveyReportServiceModel destination, ResolutionContext context)
        {
            return new SurveyReportServiceModel()
            {
                SurveyId = source.Id,
                Participants = Mapper.Map<IEnumerable<UserShortServiceModel>>(source.SurveyBlanks.Select(sb => sb.Participant)),
                QuestionsReports = Mapper.Map<IEnumerable<QuestionReportServiceModel>>(source.Questions)
            };
        }
    }
}
