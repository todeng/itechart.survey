﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using ITechArt.Survey.Domain.Enums;
using ITechArt.Survey.Domain.Models;
using ITechArt.Survey.Infra.Services.Models;
using ITechArt.Survey.Infra.Services.Models.SurveyAnswers;
using ITechArt.Survey.Infra.Services.Models.SurveyReport;

namespace ITechArt.Survey.Infra.Services.Mappings.Converters
{
    public class QuestionToQuestionReportConverter : ITypeConverter<Question, QuestionReportServiceModel>
    {
        public QuestionReportServiceModel Convert(Question source, QuestionReportServiceModel destination, ResolutionContext context)
        {
            switch (source.Type)
            {
                case QuestionType.CheckBoxList:
                    return CreateCheckBoxListQuestionReport(source);
                case QuestionType.RadioButtonList:
                    return CreateRadioButtonQuestionReport(source);
            }

            return CreateDefaultQuestionReport(source);
        }

        public QuestionReportServiceModel CreateDefaultQuestionReport(Question question)
        {
            return new QuestionReportServiceModel()
            {
                Question = Mapper.Map<QuestionServiceModel>(question)
            };
        }

        public MultiVariantQuestionReportServiceModel CreateCheckBoxListQuestionReport(Question question)
        {
            var answers = Mapper.Map<IEnumerable<SurveyAnswerServiceModel>>(question.Answers).Cast<SurveyCheckBoxListAnswerServiceModel>();
            var values = answers.SelectMany(a => a.Value).ToList();

            return new MultiVariantQuestionReportServiceModel()
            {
                Question = Mapper.Map<QuestionServiceModel>(question),
                Stats = values.GroupBy(v => v).ToDictionary(g => g.Key, g => g.Count())
            };
        }

        public MultiVariantQuestionReportServiceModel CreateRadioButtonQuestionReport(Question question)
        {
            var answers = Mapper.Map<IEnumerable<SurveyAnswerServiceModel>>(question.Answers).Cast<SurveyRadioButtonListAnswerServiceModel>();

            return new MultiVariantQuestionReportServiceModel()
            {
                Question = Mapper.Map<QuestionServiceModel>(question),
                Stats = answers.GroupBy(a => a.Value).ToDictionary(g => g.Key, q => q.Count())
            };
        }
    }
}
