﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using ITechArt.Survey.Domain.Enums;
using ITechArt.Survey.Domain.Models;
using ITechArt.Survey.Infra.Services.Models.SurveyAnswers;

namespace ITechArt.Survey.Infra.Services.Mappings.Converters
{
    public class AnswerToSpecificTypeAnswer : ITypeConverter<QuestionAnswer, SurveyAnswerServiceModel>
    {
        public SurveyAnswerServiceModel Convert(QuestionAnswer source, SurveyAnswerServiceModel destination, ResolutionContext context)
        {
            switch (source.Question.Type)
            {
                case QuestionType.String:
                    return CreateSurveyStringAnswer(source);
                case QuestionType.Empty:
                    return CreateDefaultSurveyAnswer(source);
                case QuestionType.CheckBoxList:
                    return CreateSurveyCheckBoxListAnswer(source);
                case QuestionType.RadioButtonList:
                    return CreateSurveyRadioButtonListAnswer(source);
            }

            return CreateDefaultSurveyAnswer(source);
        }

        public SurveyStringAnswerServiceModel CreateSurveyStringAnswer(QuestionAnswer answer)
        {
            return new SurveyStringAnswerServiceModel()
            {
                OriginalValue = answer.Value,
                QuestionId = answer.QuestionId,
                QuestionType = answer.Question.Type,
                Value = answer.Value
            };
        }

        public SurveyAnswerServiceModel CreateDefaultSurveyAnswer(QuestionAnswer answer)
        {
            return new SurveyAnswerServiceModel()
            {
                OriginalValue = answer.Value,
                QuestionId = answer.QuestionId,
                QuestionType = answer.Question.Type
            };
        }

        public SurveyCheckBoxListAnswerServiceModel CreateSurveyCheckBoxListAnswer(QuestionAnswer answer)
        {
            ICollection<int> value = answer.Value.Split(';').Select(int.Parse).ToList();

            return new SurveyCheckBoxListAnswerServiceModel()
            {
                OriginalValue = answer.Value,
                QuestionId = answer.QuestionId,
                QuestionType = answer.Question.Type,
                Value = value,
                Labels = value.ToDictionary(
                    v => v,
                    v => answer.Question.Variants
                        .Where(vr => vr.Id == v)
                        .Select(vr => vr.Text)
                        .FirstOrDefault()
                )
            };
        }

        public SurveyRadioButtonListAnswerServiceModel CreateSurveyRadioButtonListAnswer(QuestionAnswer answer)
        {
            var value = int.Parse(answer.Value);

            return new SurveyRadioButtonListAnswerServiceModel()
            {
                OriginalValue = answer.Value,
                QuestionId = answer.QuestionId,
                QuestionType = answer.Question.Type,
                Value = value,
                Label = answer.Question.Variants.Where(v => v.Id == value).Select(v => v.Text).FirstOrDefault()
            };
        }
    }
}
