﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using ITechArt.Survey.Domain.Enums;
using ITechArt.Survey.Domain.Models;
using ITechArt.Survey.Infra.Services.Mappings.Converters;
using ITechArt.Survey.Infra.Services.Models;
using ITechArt.Survey.Infra.Services.Models.SurveyReport;
using ITechArt.Survey.Infra.Services.Models.SurveyAnswers;

namespace ITechArt.Survey.Infra.Services.Mappings
{
    class DomainToServiceModelMappingProfile :  Profile
    {
        public DomainToServiceModelMappingProfile()
        {
            CreateMap<Survey.Domain.Models.Survey, SurveyShortServiceModel>()
                .ForMember(
                    dist => dist.ParticipantsCount,
                    x => x.MapFrom(src => src.SurveyBlanks.Select(y => y.ParticipantId).Distinct().Count())
                )
                .ForMember(dist => dist.CommentsCount, x => x.MapFrom(src => src.Comments.Count))
                .ForMember(dist => dist.Active, x => x.MapFrom(src => src.EndAt < DateTime.Now))
                .ForMember(dist => dist.CreatedBy, x => x.MapFrom(src => src.CreatedById));

            CreateMap<User, UserShortServiceModel>();
            CreateMap<Domain.Models.Survey, SurveyServiceModel>();
            CreateMap<Question, QuestionServiceModel>();
            CreateMap<QuestionAnswer, QuestionAnswerServiceModel>();
            CreateMap<QuestionVariant, QuestionVariantServiceModel>();
            CreateMap<Comment, CommentServiceModel>();
            CreateMap<SurveyBlank, SurveyBlankServiceModel>();

            CreateMap<IEnumerable<SurveyBlank>, SurveyReportServiceModel>()
                .ForMember(
                    dist => dist.Participants,
                    x => x.MapFrom(src => src.Select(y => y.Participant))
                );

            CreateMap<QuestionAnswer, SurveyAnswerServiceModel>().ConvertUsing(new AnswerToSpecificTypeAnswer());
            CreateMap<Domain.Models.Survey, SurveyReportServiceModel>().ConvertUsing(new SurveyToSurveyReportConverter());
            CreateMap<Question, QuestionReportServiceModel>().ConvertUsing(new QuestionToQuestionReportConverter());
        }

        public override string ProfileName => "DomainToServiceModelMappingProfile";
    }
}
