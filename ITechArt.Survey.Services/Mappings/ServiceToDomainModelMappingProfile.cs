﻿using AutoMapper;
using ITechArt.Survey.Domain.Models;
using ITechArt.Survey.Infra.Services.Models;

namespace ITechArt.Survey.Infra.Services.Mappings
{
    public class ServiceToDomainModelMappingProfile : Profile
    {
        public ServiceToDomainModelMappingProfile()
        {
            CreateMap<SurveyServiceModel, Domain.Models.Survey> ();
            CreateMap<QuestionServiceModel, Question>();
            CreateMap<QuestionVariantServiceModel, QuestionVariant>();
        }

        public override string ProfileName => "ServiceToDomainModelMappingProfile";
    }
}
