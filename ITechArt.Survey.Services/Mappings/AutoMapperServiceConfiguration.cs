﻿using System;
using AutoMapper;

namespace ITechArt.Survey.Infra.Services.Mappings
{
    public class AutoMapperServiceConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(ConfigAction);
        }

        public static Action<IMapperConfigurationExpression> ConfigAction = x =>
        {
            x.AddProfile<DomainToServiceModelMappingProfile>();
            x.AddProfile<ServiceToDomainModelMappingProfile>();
        };
    }
}
