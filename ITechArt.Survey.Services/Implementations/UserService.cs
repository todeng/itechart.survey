﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using ITechArt.Survey.Domain.Interfaces;
using ITechArt.Survey.Domain.Models;
using ITechArt.Survey.Infra.Services.Extensions;
using ITechArt.Survey.Infra.Services.Interfaces;
using ITechArt.Survey.Infra.Services.Models;
using ITechArt.Survey.Infra.Services.SearchCriterias;

namespace ITechArt.Survey.Infra.Services.Implementations
{
    public class UserService: IUserService
    {
        private readonly ISmgService _smgService;
        private readonly IRepository<User, string> _userRepository;

        public UserService(IUnitOfWork unitOfWork, ISmgService smgService)
        {
            _smgService = smgService;
            _userRepository = unitOfWork.Users;
        }

        public async Task<UserSearchResultServiceModel> SearchUsersAsync(ISearchCriteria searchCriteria,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var users = await _userRepository.GetQuery()
                .ApplySearch(searchCriteria)
                .Select(user => Mapper.Map<User, UserShortServiceModel>(user))
                .ToListAsync(cancellationToken);
            var count = await _userRepository.CountAsync(cancellationToken);

            return new UserSearchResultServiceModel() {Result = users, Criteria = searchCriteria, Count = count};
        }

        public async Task<IEnumerable<UserShortServiceModel>> GetUsersByDepartmentIdAsync(int departmentId,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            return await _userRepository.GetQuery()
                .Where(x => x.DeptId == departmentId)
                .Select(user => Mapper.Map<User, UserShortServiceModel>(user))
                .ToListAsync(cancellationToken);
        }

        public async Task<User> GetUserByUsernameAsync(string username,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var users = await _userRepository.FindAsync((x) => x.DomenName.ToLower() == username.ToLower(), cancellationToken);

            return users.FirstOrDefault();
        }

        public async Task<User> GetUserByUsernameAndPasswordAsync(string username, string password,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var sessionId = await _smgService.AuthenticateAsync(username, password, cancellationToken);

            return sessionId != 0 ? await GetUserByUsernameAsync(username, cancellationToken) : null;
        }

        public async Task<User> GetUserByIdAsync(string userId,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            return await _userRepository.GetByIdAsync(userId, cancellationToken);
        }

        public async Task<IEnumerable<UserShortServiceModel>> GetAllUsers(CancellationToken cancellationToken = default(CancellationToken))
        {
            return await _userRepository.GetQuery()
                .Select(user => Mapper.Map<User, UserShortServiceModel>(user))
                .ToListAsync(cancellationToken);
        }

        public Task<int> GetUsersCount()
        {
            return _userRepository.CountAsync();
        }
    }
}
