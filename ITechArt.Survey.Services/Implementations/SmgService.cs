﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ITechArt.SmgApi;
using ITechArt.SmgApi.Configuration;
using ITechArt.SmgApi.Models;
using ITechArt.Survey.Infra.Services.Interfaces;
using Microsoft.Extensions.Options;

namespace ITechArt.Survey.Infra.Services.Implementations
{
    public class SmgService: ISmgService
    {
        readonly SmgApiClient _smgApi;

        public SmgService(IOptions<SmgOptions> smgOptions)
        {
            _smgApi = new SmgApiClient(smgOptions.Value.BaseUrl);
        }

        public async Task<int> AuthenticateAsync(string username, string password, CancellationToken cancellationToken = default(CancellationToken))
        {
            var response = await _smgApi.AuthenticateAsync(username, password, cancellationToken);

            return response.SessionId;
        }

        public async Task<IEnumerable<Profile>> GetProfilesAsync(int sessionId, CancellationToken cancellationToken = default(CancellationToken))
        {
            var getEmployeesResponse = await _smgApi.GetEmployeesAsync(sessionId, cancellationToken);

            var profiles = await Task.WhenAll(getEmployeesResponse.Profiles.Select(
                async profile => await GetProfileAsync(sessionId, profile.ProfileId, cancellationToken)
            ));

            return profiles.Where(x => x != null);

        }

        public async Task<IEnumerable<Profile>> GetProfilesAsync(int sessionId, int departmentId, CancellationToken cancellationToken = default(CancellationToken))
        {
            var getEmployeesResponse = await _smgApi.GetEmployeesAsync(sessionId, departmentId, cancellationToken);

            var profiles = await Task.WhenAll(getEmployeesResponse.Profiles.Select(
                async profile => await GetProfileAsync(sessionId, profile.ProfileId, cancellationToken)
            ));

            return profiles.Where(x => x != null);
        }

        public async Task<Profile> GetProfileAsync(int sessionId, int profileId, CancellationToken cancellationToken = default(CancellationToken))
        {
            var output = await _smgApi.GetEmployeeAsync(sessionId, profileId, cancellationToken);

            return output.Profile;
        }
    }
}
