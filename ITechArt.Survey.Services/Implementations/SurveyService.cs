﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using ITechArt.Survey.Domain.Interfaces;
using ITechArt.Survey.Domain.Models;
using ITechArt.Survey.Infra.Services.Extensions;
using ITechArt.Survey.Infra.Services.Interfaces;
using ITechArt.Survey.Infra.Services.Mappings.Converters;
using ITechArt.Survey.Infra.Services.Models;
using ITechArt.Survey.Infra.Services.Models.SurveyAnswers;
using ITechArt.Survey.Infra.Services.Models.SurveyReport;
using ITechArt.Survey.Infra.Services.SearchCriterias;

namespace ITechArt.Survey.Infra.Services.Implementations
{
    public class SurveyService: ISurveyService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<Domain.Models.Survey, int> _surveyRepository;
        private readonly IRepository<Comment, int> _commentRepository;
        private readonly IRepository<QuestionAnswer, int> _questionAnswersRepository;
        private readonly IRepository<SurveyBlank, int> _surveyBlankRepository;

        public SurveyService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _surveyRepository = unitOfWork.Surveys;
            _commentRepository = unitOfWork.Comments;
            _questionAnswersRepository = unitOfWork.QuestionAnswers;
            _surveyBlankRepository = unitOfWork.SurveyBlanks;
        }

        public async Task<SurveyServiceModel> GetSurveyDetailsAsync(int id, CancellationToken cancellationToken = default(CancellationToken))
        {
            var survey = await _surveyRepository.GetQuery()
                .Include(x => x.Questions.Select(q => q.Variants))
                .Where(x => x.Id == id)
                .FirstOrDefaultAsync(cancellationToken);

            if (survey != null)
            {
                survey.Questions = survey.Questions.OrderBy(x => x.Seq).ToList();
            }

            return Mapper.Map<Domain.Models.Survey, SurveyServiceModel>(survey);
        }

        public async Task<int> AddViewAsync(int id, CancellationToken cancellationToken = default(CancellationToken))
        {
            var survey = await _surveyRepository.GetByIdAsync(id, cancellationToken);

            if (survey != null)
            {
                survey.ViewsCount = survey.ViewsCount + 1;

                _surveyRepository.Update(survey);

                await _unitOfWork.SaveAsync(cancellationToken);

                return survey.ViewsCount;
            }

            return 0;
        }


        public async Task<SurveyServiceModel> GetSurveyDetailsAndAddViewAsync(int id, CancellationToken cancellationToken = default(CancellationToken))
        {
            await AddViewAsync(id, cancellationToken);

            return await GetSurveyDetailsAsync(id, cancellationToken);
        }

        public async Task<SurveyServiceModel> UpdateSurveyAsync(SurveyServiceModel model, CancellationToken cancellationToken = default(CancellationToken))
        {
            var entitySurvey = Mapper.Map<Domain.Models.Survey>(model);
            var targetEntity = await _surveyRepository.GetQuery()
                .Where(x => x.Id == model.Id)
                .Include(s => s.Questions.Select(q => q.Variants))
                .FirstOrDefaultAsync(cancellationToken);

            entitySurvey.Questions = entitySurvey.Questions
                .Select((v, i) => {
                    v.Seq = i + 1;

                    return v;
                })
                .ToList();

            var result = _surveyRepository.UpdateGraph(targetEntity, entitySurvey);

            await _unitOfWork.SaveAsync(cancellationToken);

            return Mapper.Map<SurveyServiceModel>(result);
        }

        public async Task<SurveyBlankServiceModel> GetSurveyBlankAsync(int surveyId, string userId, CancellationToken cancellationToken = default(CancellationToken))
        {
            var surveyBlank = await _surveyBlankRepository.GetQuery()
                .Include(x => x.Answers)
                .Where(x => x.ParticipantId == userId && x.SurveyId == surveyId)
                .FirstOrDefaultAsync(cancellationToken);

            return Mapper.Map<SurveyBlankServiceModel>(surveyBlank);
        }

        public async Task<SurveyBlankServiceModel> AddSurveyAnswersAsync(int surveyId, string userId, ICollection<QuestionAnswer> answers, CancellationToken cancellationToken = default(CancellationToken))
        {
            var surveyBlank = await _surveyBlankRepository.GetQuery()
                .Include(x => x.Answers)
                .Where(x => x.ParticipantId == userId && x.SurveyId == surveyId)
                .FirstOrDefaultAsync(cancellationToken);

            if (surveyBlank != null)
            {
                _questionAnswersRepository.RemoveRange(surveyBlank.Answers);
                surveyBlank.Answers = answers;
            }
            else
            {
                surveyBlank = _surveyBlankRepository.Add(new SurveyBlank()
                {
                    Answers = answers,
                    ParticipantId = userId,
                    SurveyId = surveyId,
                    CreatedAt = DateTime.Now,
                });
            }

            await _unitOfWork.SaveAsync(cancellationToken);

            return Mapper.Map<SurveyBlankServiceModel>(surveyBlank);
        }

        public async Task<SurveySearchResultServiceModel> SearchSurveysAsync(ISearchCriteria searchCriteria = null,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var surveys  = await _surveyRepository.GetQuery()
                .Include(x => x.SurveyBlanks)
                .Include(x => x.Comments)
                .ApplySearch(searchCriteria)
                .Select(survey => Mapper.Map<SurveyShortServiceModel>(survey))
                .ToListAsync(cancellationToken);
            var count = await _surveyRepository.CountAsync(cancellationToken);

            return new SurveySearchResultServiceModel() { Result = surveys, Count = count, Criteria = searchCriteria };
        }

        public async Task<SurveySearchResultServiceModel> SearchActiveSurveysAsync(ISearchCriteria searchCriteria = null,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var surveys = await _surveyRepository.GetQuery()
                .Include(x => x.SurveyBlanks)
                .Include(x => x.Comments)
                .Where(s => s.EndAt < DateTime.Now)
                .ApplySearch(searchCriteria)
                .Select(survey => Mapper.Map<SurveyShortServiceModel>(survey))
                .ToListAsync(cancellationToken);
            var count = await _surveyRepository.CountAsync(s => s.EndAt < DateTime.Now, cancellationToken);

            return new SurveySearchResultServiceModel() { Result = surveys, Count = count, Criteria = searchCriteria };
        }

        public async Task<SurveySearchResultServiceModel> SearchInactiveSurveysAsync(ISearchCriteria searchCriteria = null,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var surveys = await _surveyRepository.GetQuery()
                .Include(x => x.SurveyBlanks)
                .Include(x => x.Comments)
                .Where(s => s.EndAt >= DateTime.Now)
                .ApplySearch(searchCriteria)
                .Select(survey => Mapper.Map<SurveyShortServiceModel>(survey))
                .ToListAsync(cancellationToken);
            var count = await _surveyRepository.CountAsync(s => s.EndAt >= DateTime.Now, cancellationToken);

            return new SurveySearchResultServiceModel() { Result = surveys, Count = count, Criteria = searchCriteria };
        }

        public async Task<SurveyServiceModel> CreateSurveyAsync(SurveyServiceModel survey, string userId, CancellationToken cancellationToken = default(CancellationToken))
        {
            var entitySurvey = Mapper.Map<Domain.Models.Survey>(survey);

            entitySurvey.CreatedAt = DateTime.Now;
            entitySurvey.CreatedById = userId;

            _surveyRepository.Add(entitySurvey);

            await _unitOfWork.SaveAsync(cancellationToken);

            return Mapper.Map<SurveyServiceModel>(entitySurvey);
        }

        public async Task DeleteSurveyAsync(int surveyId, CancellationToken cancellationToken = default(CancellationToken))
        {
            _surveyRepository.Remove(x => x.Id == surveyId);

            await _unitOfWork.SaveAsync(cancellationToken);
        }

        public async Task<CommentServiceModel> AddCommentAsync(string body, int surveyId, string authorId,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var commentEntity = _commentRepository.Add(new Comment()
            {
                SurveyId = surveyId,
                AuthorId = authorId,
                Body = body,
                CreatedAt = DateTime.Now
            });

            await _unitOfWork.SaveAsync(cancellationToken);

            return await GetCommentAsync(commentEntity.Id, cancellationToken);
        }

        public async Task<IEnumerable<CommentServiceModel>> GetCommentsAsync(int surveyId,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var comments = await _commentRepository.GetQuery()
                .Include(c => c.Author)
                .Where(c => c.SurveyId == surveyId)
                .ToListAsync(cancellationToken);

            return Mapper.Map<IEnumerable<CommentServiceModel>>(comments);
        }

        public async Task<CommentServiceModel> GetCommentAsync(int commentId,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var comment = await _commentRepository.GetQuery()
                .Include(c => c.Author)
                .Where(c => c.Id == commentId)
                .FirstOrDefaultAsync(cancellationToken);

            return Mapper.Map<CommentServiceModel>(comment);
        }

        public async Task<SurveyReportServiceModel> GetSurveyReportDataAsync(int surveyId,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            var survey = await _surveyRepository.GetQuery()
                .Where(s => s.Id == surveyId)
                .Include(s => s.SurveyBlanks.Select(sb => sb.Participant))
                .Include(s => s.Questions.Select(sb => sb.Variants))
                .Include(s => s.Questions.Select(sb => sb.Answers))
                .FirstOrDefaultAsync(cancellationToken);

            if (survey == null) return null;

            return Mapper.Map<SurveyReportServiceModel>(survey);
        }
    }
}
