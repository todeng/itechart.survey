﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using ITechArt.Survey.Domain.Interfaces;
using ITechArt.Survey.Domain.Models;
using ITechArt.Survey.Infra.Services.Interfaces;

namespace ITechArt.Survey.Infra.Services.Implementations
{
    public class QuestionService : IQuestionService
    {
        private readonly IRepository<Question, int> _questionRepository;


        public QuestionService(IUnitOfWork unitOfWork)
        {
            _questionRepository = unitOfWork.Questions;
        }

        public Task<IEnumerable<Question>> GetQuestionsBySurveyIdAsync(int surveyId, CancellationToken cancellationToken = default(CancellationToken))
        {
            return _questionRepository.GetQuery().Where(q => q.SurveyId == surveyId).ToListAsync(cancellationToken);
        }
    }
}
