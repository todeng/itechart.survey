﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using ITechArt.SmgApi.Models;

namespace ITechArt.Survey.Infra.Services.Interfaces
{
    public interface ISmgService
    {
        Task<int> AuthenticateAsync(string username, string password, CancellationToken cancellationToken = default(CancellationToken));
        Task<IEnumerable<Profile>> GetProfilesAsync(int sessionId, CancellationToken cancellationToken = default(CancellationToken));
        Task<IEnumerable<Profile>> GetProfilesAsync(int sessionId, int departmentId, CancellationToken cancellationToken = default(CancellationToken));
    }
}
