﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using ITechArt.Survey.Domain.Models;
using ITechArt.Survey.Infra.Services.Models;
using ITechArt.Survey.Infra.Services.Models.SurveyReport;
using ITechArt.Survey.Infra.Services.SearchCriterias;

namespace ITechArt.Survey.Infra.Services.Interfaces
{
    public interface ISurveyService
    {
        Task<SurveyServiceModel> GetSurveyDetailsAsync(int id, CancellationToken cancellationToken = default(CancellationToken));
        Task<int> AddViewAsync(int id, CancellationToken cancellationToken = default(CancellationToken));
        Task<SurveyServiceModel> GetSurveyDetailsAndAddViewAsync(int id, CancellationToken cancellationToken = default(CancellationToken));
        Task<SurveyBlankServiceModel> GetSurveyBlankAsync(int surveyId, string userId, CancellationToken cancellationToken = default(CancellationToken));
        Task<SurveyBlankServiceModel> AddSurveyAnswersAsync(int surveyId, string userId, ICollection<QuestionAnswer> answers, CancellationToken cancellationToken = default(CancellationToken));

        Task<SurveyServiceModel> UpdateSurveyAsync(SurveyServiceModel model, CancellationToken cancellationToken = default(CancellationToken));

        Task<SurveySearchResultServiceModel> SearchSurveysAsync(ISearchCriteria searchCriteria = null, CancellationToken cancellationToken = default(CancellationToken));
        Task<SurveySearchResultServiceModel> SearchActiveSurveysAsync(ISearchCriteria searchCriteria = null, CancellationToken cancellationToken = default(CancellationToken));
        Task<SurveySearchResultServiceModel> SearchInactiveSurveysAsync(ISearchCriteria searchCriteria = null, CancellationToken cancellationToken = default(CancellationToken));

        Task<SurveyServiceModel> CreateSurveyAsync(SurveyServiceModel survey, string userId, CancellationToken cancellationToken = default(CancellationToken));

        Task DeleteSurveyAsync(int surveyId, CancellationToken cancellationToken = default(CancellationToken));

        Task<CommentServiceModel> AddCommentAsync(string body, int surveyId, string authorId, CancellationToken cancellationToken = default(CancellationToken));
        Task<IEnumerable<CommentServiceModel>> GetCommentsAsync(int surveyId, CancellationToken cancellationToken = default(CancellationToken));

        Task<SurveyReportServiceModel> GetSurveyReportDataAsync(int surveyId, CancellationToken cancellationToken = default(CancellationToken));
    }
}
