﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using ITechArt.Survey.Domain.Models;
using ITechArt.Survey.Infra.Services.Models;
using ITechArt.Survey.Infra.Services.SearchCriterias;

namespace ITechArt.Survey.Infra.Services.Interfaces
{
    public interface IUserService
    {
        Task<UserSearchResultServiceModel> SearchUsersAsync(ISearchCriteria searchCriteria, CancellationToken cancellationToken = default(CancellationToken));
        Task<IEnumerable<UserShortServiceModel>> GetAllUsers(CancellationToken cancellationToken = default(CancellationToken));
        Task<User> GetUserByIdAsync(string userId, CancellationToken cancellationToken = default(CancellationToken));
        Task<User> GetUserByUsernameAsync(string username, CancellationToken cancellationToken = default(CancellationToken));
        Task<User> GetUserByUsernameAndPasswordAsync(string username, string password, CancellationToken cancellationToken = default(CancellationToken));
        Task<IEnumerable<UserShortServiceModel>> GetUsersByDepartmentIdAsync(int departmentId, CancellationToken cancellationToken = default(CancellationToken));
        Task<int> GetUsersCount();
    }
}
