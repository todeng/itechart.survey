﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using ITechArt.Survey.Domain.Models;

namespace ITechArt.Survey.Infra.Services.Interfaces
{
    public interface IQuestionService
    {
        Task<IEnumerable<Question>> GetQuestionsBySurveyIdAsync(int surveyId, CancellationToken cancellationToken = default(CancellationToken));
    }
}
