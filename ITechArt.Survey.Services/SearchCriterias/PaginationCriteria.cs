﻿using ITechArt.Survey.Domain.Interfaces;

namespace ITechArt.Survey.Infra.Services.SearchCriterias
{
    public class PaginationCriteria : ISearchCriteria
    {
        public int PageNumber { get; set; }
        public int? PageSize { get; set; }

        public IQuery<T> ApplyQuery<T>(IQuery<T> query) where T : class
        {
            return PageSize.HasValue ? query.Skip(PageNumber * PageSize.Value).Take(PageSize.Value) : query;
        }
    }
}
