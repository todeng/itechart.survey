﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using ITechArt.Survey.Domain.Interfaces;

namespace ITechArt.Survey.Infra.Services.SearchCriterias
{
    public class FindCriteria : ISearchCriteria
    {
        public string FindQuery { get; set; }

        private Dictionary<string, string> ParseFindQuery(string findQuery)
        {
            if (string.IsNullOrEmpty(findQuery)) return null;

            return findQuery.Split(';')
                .Where(x => !string.IsNullOrWhiteSpace(x))
                .Select(x => x.Split(':'))
                .Where(x => !string.IsNullOrWhiteSpace(x[0]) && !string.IsNullOrWhiteSpace(x[1]))
                .ToDictionary(x => x[0],  x => x[1]);
        }

        public IQuery<T> ApplyQuery<T>(IQuery<T> query) where T : class
        {
            var findDict = ParseFindQuery(FindQuery);

            if (findDict == null) return query;
            var type = typeof(T);

            foreach (var find in findDict)
            {
                var property = type.GetProperty(find.Key,
                    BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);

                if (property == null) continue;

                var propertyType = property.PropertyType;

                if (propertyType == typeof(string))
                {
                    var parameterExp = Expression.Parameter(typeof(T));
                    var propertyExp = Expression.Property(parameterExp, property);
                    var containsExp = Expression.Call(propertyExp, typeof(string).GetMethod("Contains"), Expression.Constant(find.Value));

                    var lambda = Expression.Lambda<Func<T, bool>>(containsExp, parameterExp);

                    query = query.Where(lambda);
                }

                if (propertyType == typeof(int) && int.TryParse(find.Value, out var intValue))
                {
                    var parameterExp = Expression.Parameter(typeof(T));
                    var propertyExp = Expression.Property(parameterExp, property);
                    var equalExp = Expression.Equal(propertyExp, Expression.Constant(intValue));

                    var lambda = Expression.Lambda<Func<T, bool>>(equalExp, parameterExp);

                    query = query.Where(lambda);
                }

                if (propertyType == typeof(bool) && bool.TryParse(find.Value, out var boolValue))
                {
                    var parameterExp = Expression.Parameter(typeof(T));
                    var propertyExp = Expression.Property(parameterExp, property);
                    var equalExp = Expression.Equal(propertyExp, Expression.Constant(boolValue));

                    var lambda = Expression.Lambda<Func<T, bool>>(equalExp, parameterExp);

                    query = query.Where(lambda);
                }
            }

            return query;
        }
    }
}
