﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using ITechArt.Survey.Domain.Interfaces;
using ITechArt.Survey.Infra.Services.Extensions;

namespace ITechArt.Survey.Infra.Services.SearchCriterias
{
    internal enum OrderType
    {
        Desc,
        Asc
    }

    public class OrderByCriteria : ISearchCriteria
    {
        public string OrderByQuery { get; set; }

        private Dictionary<string, OrderType> ParseOrder(string orderQuery)
        {
            if (string.IsNullOrEmpty(orderQuery)) return null;

            return orderQuery.Split(';')
                .Where(x => !string.IsNullOrWhiteSpace(x))
                .Select(x => x.Split(':'))
                .ToDictionary(
                    x => x[0],
                    x => x.Length > 1 && !string.IsNullOrWhiteSpace(x[1]) ? x[1].ToEnum(OrderType.Desc) : OrderType.Desc
                );
        }

        public IQuery<T> ApplyQuery<T>(IQuery<T> query) where T : class
        {
            var orderDict = ParseOrder(OrderByQuery);

            if (orderDict == null) return query;
            var type = typeof(T);

            foreach (var order in orderDict)
            {
                if (type.GetProperty(order.Key,
                        BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance) == null) continue;
                var orderedQuery = query as IOrderedQuery<T>;

                switch (order.Value)
                {
                    case OrderType.Asc:
                        query = orderedQuery == null ? query.OrderBy(order.Key) : orderedQuery.ThenBy(order.Key);
                        break;
                    case OrderType.Desc:
                    default:
                        query = orderedQuery == null ? query.OrderByDescending(order.Key) : orderedQuery.ThenByDescending(order.Key);
                        break;
                }
            }

            return query;
        }
    }
}
