﻿using ITechArt.Survey.Domain.Interfaces;
using ITechArt.Survey.Infra.Services.Extensions;

namespace ITechArt.Survey.Infra.Services.SearchCriterias
{
    public class SearchCriteria : ISearchCriteria
    {
        public string Order { get; set; }
        public string Find { get; set; }
        public int PageNumber { get; set; }
        public int? PageSize { get; set; }

        public IQuery<T> ApplyQuery<T>(IQuery<T> query) where T : class
        {
            query = query.ApplySearch(new FindCriteria() { FindQuery = this.Find });
            query = query.ApplySearch(new OrderByCriteria() { OrderByQuery = this.Order });
            query = query.ApplySearch(new PaginationCriteria() { PageNumber = this.PageNumber, PageSize = this.PageSize });

            return query;
        }
    }
}
