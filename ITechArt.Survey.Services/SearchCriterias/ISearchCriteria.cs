﻿using ITechArt.Survey.Domain.Interfaces;

namespace ITechArt.Survey.Infra.Services.SearchCriterias
{
    public interface ISearchCriteria
    {
        IQuery<T> ApplyQuery<T>(IQuery<T> query) where T : class;
    }
}
