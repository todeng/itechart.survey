﻿namespace ITechArt.SmgApi.Models
{
    public class ProfileShort: ProfileExtraShort
    {
        public string Position { get; set; }
        public string Room { get; set; }
        public int DeptId { get; set; }
        public string Photo { get; set; }
    }
}
