﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ITechArt.SmgApi.Models
{
    public class BaseOutput
    {
        public string ErrorCode { get; set; }
        public string Permission { get; set; }
    }
}
