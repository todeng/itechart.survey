﻿using System.Collections.Generic;

namespace ITechArt.SmgApi.Models
{
    public class GetDepartmentsOutput : BaseOutput
    {
        public IEnumerable<Department> Depts { get; set; }
    }
}
