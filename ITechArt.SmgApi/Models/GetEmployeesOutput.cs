﻿using System.Collections.Generic;

namespace ITechArt.SmgApi.Models
{
    public class GetEmployeesOutput: BaseOutput
    {
        public IEnumerable<ProfileShort> Profiles { get; set; }
    }
}
