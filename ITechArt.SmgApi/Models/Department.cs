﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ITechArt.SmgApi.Models
{
    public class Department
    {
        public int Id { get; set; }
        public string DepCode { get; set; }
        public string Name { get; set; }
        public int NumUsers { get; set; }
    }
}
