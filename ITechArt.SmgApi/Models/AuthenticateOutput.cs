﻿namespace ITechArt.SmgApi.Models
{
    public class AuthenticateOutput: BaseOutput
    {
        public int SessionId { get; set; }
    }
}
