﻿namespace ITechArt.SmgApi.Models
{
    public class ProfileExtraShort
    {
        public int ProfileId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FirstNameEng { get; set; }
        public string LastNameEng { get; set; }
        public bool IsEnabled { get; set; }
    }
}
