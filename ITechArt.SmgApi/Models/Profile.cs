﻿using System;

namespace ITechArt.SmgApi.Models
{
    public class Profile: ProfileShort
    {
        public string Rank { get; set; }
        public string MiddleName { get; set; }
        public DateTime Birthday { get; set; }
        public string Skype { get; set; }
        public string Phone { get; set; }
        public string DomenName { get; set; }
        public string Email { get; set; }
        public string Image { get; set; }
    }
}
