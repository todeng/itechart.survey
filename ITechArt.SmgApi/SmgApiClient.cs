﻿using ITechArt.SmgApi.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ITechArt.SmgApi.Extensions;

namespace ITechArt.SmgApi
{
    public class SmgApiClient
    {
        private readonly string _baseUrl = "https://smg.itechart-group.com/MobileServiceNew/MobileService.svc";

        public SmgApiClient()
        {
        }

        public SmgApiClient(string baseUrl)
        {
            _baseUrl = baseUrl;
        }

        public async Task<AuthenticateOutput> AuthenticateAsync(string username, string password, CancellationToken cancellationToken = default(CancellationToken))
        {
            var url = _baseUrl + "/PostAuthenticate";
            var client = new HttpClient();
            var body = JsonConvert.SerializeObject(new { Username = username, Password = password });

            var response = await client.PostAsync(url, new StringContent(body, Encoding.UTF8, "application/json"), cancellationToken);
            var content = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<AuthenticateOutput>(content);
        }


        public async Task<GetDepartmentsOutput> GetDepartmentsAsync(int sessionId, CancellationToken cancellationToken = default(CancellationToken))
        {
            var url = _baseUrl + "/GetAllDepartments";
            var urlWithQuery = url.AddQueyString("sessionId", sessionId.ToString());
            var client = new HttpClient();

            var response = await client.GetAsync(urlWithQuery, cancellationToken);
            var content = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<GetDepartmentsOutput>(content);
        }


        public async Task<GetEmployeesOutput> GetEmployeesAsync(int sessionId, CancellationToken cancellationToken = default(CancellationToken))
        {
            var url = _baseUrl + "/GetAllEmployees";
            var urlWithQuery = url.AddQueyString("sessionId", sessionId.ToString());

            var client = new HttpClient();
            var response = await client.GetAsync(urlWithQuery, cancellationToken);
            var content = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<GetEmployeesOutput>(content);
        }


        public async Task<GetEmployeesOutput> GetEmployeesAsync(int sessionId, int departmentId, CancellationToken cancellationToken = default(CancellationToken))
        {
            var url = _baseUrl + "/GetEmployeesByDeptId";
            var urlWithQuery = url.AddQueyString(new Dictionary<string, string>()
                {
                    { "sessionId", sessionId.ToString() },
                    { "departmentId", departmentId.ToString() },
                });

            var client = new HttpClient();
            var response = await client.GetAsync(urlWithQuery, cancellationToken);
            var content = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<GetEmployeesOutput>(content);
        }

        public async Task<GetEmployeeOutput> GetEmployeeAsync(int sessionId, int profileId, CancellationToken cancellationToken = default(CancellationToken))
        {
            var url = _baseUrl + "/GetEmployeeDetails";
            var urlWithQuery = url.AddQueyString(new Dictionary<string, string>()
                {
                    { "sessionId", sessionId.ToString() },
                    { "profileId", profileId.ToString() }
                });

            var client = new HttpClient();
            var response = await client.GetAsync(urlWithQuery, cancellationToken);
            var content = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<GetEmployeeOutput>(content);
        }
    }
}
