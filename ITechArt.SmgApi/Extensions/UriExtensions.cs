﻿using System;
using System.Collections.Generic;

namespace ITechArt.SmgApi.Extensions
{
    public static class UriExtension
    {
        public static Uri AddQueyString(this Uri url, string queryStringKey, string queryStringValue)
        {
            return new Uri(url.PathAndQuery.AddQueyString(new Dictionary<string, string>() {
                { queryStringKey, queryStringValue }
            }));
        }

        public static Uri AddQueyString(this Uri url, Dictionary<string, string> dict)
        {
            return new Uri(url.PathAndQuery.AddQueyString(dict));
        }
    }
}
