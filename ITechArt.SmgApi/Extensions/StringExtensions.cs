﻿using System.Collections.Generic;
using System.Linq;

namespace ITechArt.SmgApi.Extensions
{
    public static class StringExtension
    {
        public static string AddQueyString(this string url, string queryStringKey, string queryStringValue)
        {
            return AddQueyString(url, new Dictionary<string, string>() {
                { queryStringKey, queryStringValue }
            });
        }

        public static string AddQueyString(this string url, Dictionary<string, string> dict)
        {
            var query = dict.Select(i => i.Key + "=" + i.Value).Aggregate((current, next) => current + "&" + next);
            var segments = url.Split('?');
            var queryString = segments.Length > 1 ? "&" : "?";

            return url + queryString + query;
        }
    }
}
