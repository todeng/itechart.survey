﻿namespace ITechArt.Survey.Domain.Enums
{
    public enum QuestionType
    {
        Empty = 0,
        String = 1,
        CheckBoxList = 2,
        RadioButtonList = 3
    }
}
