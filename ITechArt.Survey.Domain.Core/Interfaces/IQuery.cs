﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace ITechArt.Survey.Domain.Interfaces
{
    public interface IQuery<TEntity>
    {
        IQuery<TResult> Select<TResult>(Expression<Func<TEntity, TResult>> selector) where TResult : class;
        IQuery<TEntity> Where(Expression<Func<TEntity, bool>> predicate);
        IOrderedQuery<TEntity> OrderBy<TKey>(Expression<Func<TEntity, TKey>> keySelector);
        IOrderedQuery<TEntity> OrderByDescending<TKey>(Expression<Func<TEntity, TKey>> keySelector);
        IQuery<TEntity> Include(Expression<Func<TEntity, object>> include);
        IQuery<TEntity> Take(int count);
        IQuery<TEntity> Skip(int count);

        int Count();
        Task<int> CountAsync(CancellationToken cancellationToken = default(CancellationToken));
        IEnumerable<TEntity> ToList();
        Task<IEnumerable<TEntity>> ToListAsync(CancellationToken cancellationToken = default(CancellationToken));
        TEntity FirstOrDefault();
        Task<TEntity> FirstOrDefaultAsync(CancellationToken cancellationToken = default(CancellationToken));
    }
}
