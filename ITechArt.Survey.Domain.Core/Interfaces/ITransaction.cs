﻿using System;

namespace ITechArt.Survey.Domain.Interfaces
{
    public interface ITransaction : IDisposable
    {
        void Commit();

        void Rollback();
    }
}
