﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace ITechArt.Survey.Domain.Interfaces
{
    public interface IRepository<TEntity, in TPrimaryKey> where TEntity : IBaseEntity<TPrimaryKey>
    {
        TEntity GetById(TPrimaryKey id);
        Task<TEntity> GetByIdAsync(TPrimaryKey id, CancellationToken cancellationToken = default(CancellationToken));

        IEnumerable<TEntity> GetAll();
        Task<IEnumerable<TEntity>> GetAllAsync(CancellationToken cancellationToken = default(CancellationToken));

        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate);
        Task<IEnumerable<TEntity>> FindAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken cancellationToken = default(CancellationToken));

        TEntity Add(TEntity obj);
        IEnumerable<TEntity> AddRange(IEnumerable<TEntity> objs);

        TEntity Update(TEntity obj);
        IEnumerable<TEntity> UpdateRange(IEnumerable<TEntity> objs);

        TEntity UpdateGraph(TEntity targetEntitie, TEntity sourceEntity);

        void Remove(TEntity obj);
        void Remove(Expression<Func<TEntity, bool>> predicate);
        void RemoveRange(IEnumerable<TEntity> objs);

        int Count();
        int Count(Expression<Func<TEntity, bool>> predicate);
        Task<int> CountAsync(CancellationToken cancellationToken = default(CancellationToken));
        Task<int> CountAsync(Expression<Func<TEntity, bool>> predicate, CancellationToken cancellationToken = default(CancellationToken));

        IQuery<TEntity> GetQuery();
    }
}
