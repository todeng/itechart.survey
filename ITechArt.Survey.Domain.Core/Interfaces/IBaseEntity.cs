﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ITechArt.Survey.Domain.Interfaces
{
    public interface IBaseEntity<TPrimaryKey>
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        TPrimaryKey Id { get; set; }
    }
}
