﻿using System;
using System.Linq.Expressions;

namespace ITechArt.Survey.Domain.Interfaces
{
    public interface IOrderedQuery<TEntity> : IQuery<TEntity>
    {
        IQuery<TEntity> ThenBy<TKey>(Expression<Func<TEntity, TKey>> keySelector);
        IQuery<TEntity> ThenByDescending<TKey>(Expression<Func<TEntity, TKey>> keySelector);
    }
}
