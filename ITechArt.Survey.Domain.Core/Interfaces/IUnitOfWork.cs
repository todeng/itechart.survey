﻿using System.Threading;
using System.Threading.Tasks;
using ITechArt.Survey.Domain.Models;

namespace ITechArt.Survey.Domain.Interfaces
{
    public interface IUnitOfWork
    {
        IRepository<User, string> Users { get; }
        IRepository<Survey.Domain.Models.Survey, int> Surveys { get; }
        IRepository<QuestionVariant, int> QuestionVariants { get; }
        IRepository<QuestionAnswer, int> QuestionAnswers { get; }
        IRepository<Question, int> Questions { get; }
        IRepository<Comment, int> Comments { get; }
        IRepository<SurveyBlank, int> SurveyBlanks { get; }

        int Save();
        Task<int> SaveAsync(CancellationToken cancellationToken = default(CancellationToken));
        ITransaction BeginTrainsaction();

        IRepository<TEntity, TKey> GetRepository<TEntity, TKey>()
            where TEntity : class, IBaseEntity<TKey>;
    }
}
