﻿namespace ITechArt.Survey.Domain.Constants
{
    public static class UserRoles
    {
        public static string Administrator => "Administrator";
        public static string CommomUser => "CommomUser";
    }
}
