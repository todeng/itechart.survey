﻿namespace ITechArt.Survey.Domain.Models
{
    public class QuestionVariant : BaseEntity<int>
    {
        public string Text { get; set; }

        public virtual Question Question { get; set; }

        public virtual int QuestionId { get; set; }
    }
}
