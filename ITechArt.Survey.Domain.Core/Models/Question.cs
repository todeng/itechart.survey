﻿using System.Collections.Generic;
using ITechArt.Survey.Domain.Enums;

namespace ITechArt.Survey.Domain.Models
{
    public class Question : BaseEntity<int>
    {
        public string Text { get; set; }
        public QuestionType Type { get; set; }
        public virtual ICollection<QuestionVariant> Variants { get; set; }
        public virtual ICollection<QuestionAnswer> Answers { get; set; }
        public Survey Survey { get; set; }
        public int SurveyId { get; set; }
        public bool IsRequired { get; set; }
        public int Seq { get; set; }

        public Question()
        {
            Variants = new List<QuestionVariant>();
            Answers = new List<QuestionAnswer>();
        }
    }
}
