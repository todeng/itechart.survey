﻿using System;

namespace ITechArt.Survey.Domain.Models
{
    public class QuestionAnswer : BaseEntity<int>
    {
        public string Value { get; set; }
        public Question Question { get; set; }
        public int QuestionId { get; set; }
        public SurveyBlank SurveyBlank { get; set; }
        public int SurveyBlankId { get; set; }
    }
}
