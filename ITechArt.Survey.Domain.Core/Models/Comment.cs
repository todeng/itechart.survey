﻿using System;

namespace ITechArt.Survey.Domain.Models
{
    public class Comment : BaseEntity<int>
    {
        public string Body { get; set; }
        public DateTime CreatedAt { get; set; }
        public User Author { get; set; }
        public string AuthorId { get; set; }
        public Survey Survey { get; set; }
        public int SurveyId { get; set; }
    }
}
