﻿using System;
using System.Collections.Generic;

namespace ITechArt.Survey.Domain.Models
{
    public class Survey : BaseEntity<int>
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public virtual ICollection<Question> Questions { get; set; }
        public virtual ICollection<SurveyBlank> SurveyBlanks { get; set; }
        public DateTime? StartAt { get; set; }
        public DateTime? EndAt { get; set; }
        public int ViewsCount { get; set; }
        public User CreatedBy { get; set; }
        public string CreatedById { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public DateTime CreatedAt { get; set; }

        public Survey()
        {
            Questions = new List<Question>();
            Comments = new List<Comment>();
            SurveyBlanks = new List<SurveyBlank>();
        }
    }
}
