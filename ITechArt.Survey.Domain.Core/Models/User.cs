﻿using System;
using System.Collections.Generic;
using ITechArt.Survey.Domain.Constants;

namespace ITechArt.Survey.Domain.Models
{
    public class User : BaseEntity<string>
    {
        public string DomenName { get; set; }
        public int ProfileId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FirstNameEng { get; set; }
        public string LastNameEng { get; set; }
        public bool IsEnabled { get; set; }
        public string Position { get; set; }
        public string Room { get; set; }
        public int DeptId { get; set; }
        public string Photo { get; set; }
        public string Rank { get; set; }
        public string MiddleName { get; set; }
        public DateTime Birthday { get; set; }
        public string Skype { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }

        public ICollection<QuestionAnswer> QuestionAnswers { get; set; }
        public User()
        {
            QuestionAnswers = new List<QuestionAnswer>();
            Role = UserRoles.CommomUser;
        }
    }
}
