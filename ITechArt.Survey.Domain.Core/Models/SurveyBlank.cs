﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ITechArt.Survey.Domain.Models
{
    public class SurveyBlank: BaseEntity<int>
    {
        public Survey Survey { get; set; }
        public int SurveyId { get; set; }
        public virtual ICollection<QuestionAnswer> Answers { get; set; }
        public User Participant { get; set; }
        public string ParticipantId { get; set; }

        public DateTime CreatedAt { get; set; }

        public SurveyBlank()
        {
            Answers = new List<QuestionAnswer>();
        }
    }
}
