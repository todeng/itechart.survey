﻿using System.IdentityModel.Tokens.Jwt;
using ITechArt.Survey.Domain.Models;
using Microsoft.IdentityModel.Tokens;

namespace ITechArt.Survey.Infra.Identity.Providers
{
    public interface ITokenProvider
    {
        JwtSecurityToken CreateToken(User user);

        TokenValidationParameters GetValidationParameters();
    }
}
