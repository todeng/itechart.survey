﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using ITechArt.Survey.Domain.Constants;
using ITechArt.Survey.Domain.Models;
using ITechArt.Survey.Infra.Identity.Claims;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace ITechArt.Survey.Infra.Identity.Providers
{
    public class TokenProvider : ITokenProvider
    {
        private readonly SecurityKey _signingKey;
        private readonly string _issuer;
        private readonly string _audience;
        private readonly int _lifetime;

        public TokenProvider(IConfiguration configuration)
        {
            var secretKey = configuration["jwt:secretKey"];
            _signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(secretKey));
            _issuer = configuration["jwt:issuer"];
            _audience = configuration["jwt:audience"];
            _lifetime = int.Parse(configuration["jwt:lifetime"]);
        }

        public JwtSecurityToken CreateToken(User user)
        {
            var now = DateTime.UtcNow;

            return new JwtSecurityToken(
                issuer: _issuer,
                audience: _audience,
                notBefore: now,
                claims: GetClaims(user),
                expires: now.Add(TimeSpan.FromMinutes(_lifetime)),
                signingCredentials: new SigningCredentials(_signingKey, SecurityAlgorithms.HmacSha256)
            );
        }

        public TokenValidationParameters GetValidationParameters()
        {
            return new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = _issuer,
                ValidateAudience = true,
                ValidAudience = _audience,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = _signingKey,
                ValidateLifetime = true,
                ClockSkew = TimeSpan.Zero,
            };
        }

        private List<Claim> GetClaims(User user)
        {
            var now = DateTime.UtcNow;

            return new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Iat, now.ToUniversalTime().ToString(), ClaimValueTypes.Integer64),
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.DomenName),
                new Claim(TokenClaims.ProfileId, user.ProfileId.ToString()),
                new Claim(TokenClaims.UserId, user.Id),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, user.Role ?? UserRoles.CommomUser)
            };
        }
    }
}
