﻿namespace ITechArt.Survey.Infra.Identity.Claims
{
    public static class TokenClaims
    {
        public const string UserId = "itechart/userid";
        public const string ProfileId = "itechart/profileid";
    }
}
