﻿using System.Security.Claims;
using System.Security.Principal;
using ITechArt.Survey.Infra.Identity.Claims;

namespace ITechArt.Survey.Infra.Identity.Extensions
{
    public static class IdentityExtensions
    {
        public static string GetUserId(this IPrincipal principal)
        {
            return principal.Identity.FirstOrNull(TokenClaims.UserId);
        }

        public static string GetProfileId(this IPrincipal principal)
        {
            return principal.Identity.FirstOrNull(TokenClaims.ProfileId);
        }

        public static string FirstOrNull(this IIdentity identity, string claimType)
        {
            var claimsIdentity = identity as ClaimsIdentity;

            return claimsIdentity?.FirstOrNull(claimType);
        }

        public static string FirstOrNull(this ClaimsIdentity identity, string claimType)
        {
            var val = identity.FindFirst(claimType);

            return val?.Value;
        }
    }
}