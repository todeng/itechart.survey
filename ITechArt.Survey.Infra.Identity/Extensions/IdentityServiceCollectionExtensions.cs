﻿using System;
using System.Text;
using ITechArt.Survey.Infra.Identity.Providers;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace ITechArt.Survey.Infra.Identity.Extensions
{
    public static class IdentityServiceCollectionExtensions
    {
        public static IServiceCollection AddJwtAuthentication(this IServiceCollection services, IConfiguration configuration)
        {
            var tokenProvider = new TokenProvider(configuration);

            services.AddSingleton<ITokenProvider>(tokenProvider);

            services.AddAuthentication(o =>
            {
                o.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                o.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                o.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(o =>
            {
                o.RequireHttpsMetadata = false;
                o.TokenValidationParameters = tokenProvider.GetValidationParameters();
            });

            services.AddTransient<ITokenProvider, TokenProvider>();

            return services;
        }
}
}
